#adresseip

INSERT INTO `epresence`.`adresseip` VALUES ('10', '10.3.110.60');
INSERT INTO `epresence`.`adresseip` VALUES ('11', '10.3.110.53');
INSERT INTO `epresence`.`adresseip` VALUES ('12', '10.3.110.62');
INSERT INTO `epresence`.`adresseip` VALUES ('13', '10.3.110.63');
INSERT INTO `epresence`.`adresseip` VALUES ('14', '10.3.110.64');
INSERT INTO `epresence`.`adresseip` VALUES ('15', '10.3.110.65');
INSERT INTO `epresence`.`adresseip` VALUES ('16', '10.3.110.66');
INSERT INTO `epresence`.`adresseip` VALUES ('17', '10.3.110.67');
INSERT INTO `epresence`.`adresseip` VALUES ('18', '10.3.110.68');
INSERT INTO `epresence`.`adresseip` VALUES ('19', '10.3.110.69');
INSERT INTO `epresence`.`adresseip` VALUES ('20', '10.3.110.70');
INSERT INTO `epresence`.`adresseip` VALUES ('21', '10.3.110.71');
INSERT INTO `epresence`.`adresseip` VALUES ('22', '10.3.110.72');
INSERT INTO `epresence`.`adresseip` VALUES ('23', '10.3.110.73');
INSERT INTO `epresence`.`adresseip` VALUES ('24', '10.3.110.74');
INSERT INTO `epresence`.`adresseip` VALUES ('25', '10.3.110.75');
INSERT INTO `epresence`.`adresseip` VALUES ('26', '10.3.110.76');
INSERT INTO `epresence`.`adresseip` VALUES ('27', '10.3.110.77');

#Utilisateur
INSERT INTO `epresence`.`utilisateur` (`id`, `nom`, `prenom`, `mail`, `motdepasse`, `role`, `dateInscription`) VALUES ('1000', 'Dominique', 'Clara', 'claradomonique@projet.com', AES_ENCRYPT('pwd1', 'MySecret'), 'administrateur', '2013-09-25 00:00:00');
INSERT INTO `epresence`.`utilisateur` (`id`, `nom`, `prenom`, `mail`, `motdepasse`, `role`, `dateInscription`) VALUES ('1001', 'Lurus', 'Fabienne', 'fabiennelurus@projet.com', AES_ENCRYPT('pwd2', 'MySecret'), 'administrateur', '2013-10-25 00:00:00');
INSERT INTO `epresence`.`utilisateur` (`id`, `nom`, `prenom`, `mail`, `motdepasse`, `role`, `dateInscription`) VALUES ('1002', 'Lutil', 'Sarah', 'sarahlutil@projet.com', AES_ENCRYPT('pwd3', 'MySecret'), 'formateur', '2015-08-30 00:00:00');
INSERT INTO `epresence`.`utilisateur` (`id`, `nom`, `prenom`, `mail`, `motdepasse`, `role`, `dateInscription`) VALUES ('1003', 'Poicalendriers', 'Patrick', 'patrickpois@projet.com', AES_ENCRYPT('pwd4', 'MySecret'), 'formateur', '2015-08-28 00:00:00');
INSERT INTO `epresence`.`utilisateur` (`id`, `nom`, `prenom`, `mail`, `motdepasse`, `role`, `dateInscription`) VALUES ('1004', 'Renoir', 'Gilles', 'gillesrenoir@projet.com', AES_ENCRYPT('pwd5', 'MySecret'), 'formateur', '2015-08-16 00:00:00');
INSERT INTO `epresence`.`utilisateur` (`id`, `nom`, `prenom`, `mail`, `motdepasse`, `role`, `dateInscription`) VALUES ('1005', 'Chaves', 'Hugo', 'hugochaves@projet.com', AES_ENCRYPT('pwd6', 'MySecret'), 'stagiaire', '2015-08-26 00:00:00');
INSERT INTO `epresence`.`utilisateur` (`id`, `nom`, `prenom`, `mail`, `motdepasse`, `role`, `dateInscription`) VALUES ('1006', 'Henry', 'Thierry', 'thierryhenry@projet.com', AES_ENCRYPT('pwd7', 'MySecret'), 'stagiaire', '2015-08-27 00:00:00');
INSERT INTO `epresence`.`utilisateur` (`id`, `nom`, `prenom`, `mail`, `motdepasse`, `role`, `dateInscription`) VALUES ('1007', 'Abenzo', 'Lucien', 'lucienabenzo@projet.com', AES_ENCRYPT('pwd8', 'MySecret'),'stagiaire', '2015-08-29 00:00:00');
INSERT INTO `epresence`.`utilisateur` (`id`, `nom`, `prenom`, `mail`, `motdepasse`, `role`, `dateInscription`) VALUES ('1008', 'Nimos', 'Fabienne', 'fabiennenimos@projet.com', AES_ENCRYPT('pwd9', 'MySecret'), 'stagiaire', '2015-08-10 00:00:00');
INSERT INTO `epresence`.`utilisateur` (`id`, `nom`, `prenom`, `mail`, `motdepasse`, `role`, `dateInscription`) VALUES ('1009', 'Ahlmed', 'Rhana', 'rhanaahmed@projet.com',  AES_ENCRYPT('pwd10', 'MySecret'), 'stagiaire', '2015-08-11 00:00:00');
INSERT INTO `epresence`.`utilisateur` (`id`, `nom`, `prenom`, `mail`, `motdepasse`, `role`, `dateInscription`) VALUES ('1010', 'Dos Santos', 'Lara', 'laradossantos', AES_ENCRYPT('pwd11', 'MySecret'), 'stagiaire', '2015-08-12 00:00:00');
INSERT INTO `epresence`.`utilisateur` (`id`, `nom`, `prenom`, `mail`, `motdepasse`, `role`, `dateInscription`) VALUES ('1011', 'ElesGarcon', 'Helene', 'heleneelesgarcon@projet.com', AES_ENCRYPT('pwd12', 'MySecret'), 'stagiaire', '2015-08-08 00:00:00');
INSERT INTO `epresence`.`utilisateur` (`id`, `nom`, `prenom`, `mail`, `motdepasse`, `role`, `dateInscription`) VALUES ('1012', 'Chaval', 'Sebastien', 'sebastienchaval@projet.com', AES_ENCRYPT('pwd13', 'MySecret'), 'stagiaire', '2015-08-13 00:00:00');
INSERT INTO `epresence`.`utilisateur` (`id`, `nom`, `prenom`, `mail`, `motdepasse`, `role`, `dateInscription`) VALUES ('1013', 'Juraver', 'Hugo', 'juravergeslin.hugo@gmail.com', AES_ENCRYPT('pwd14', 'MySecret'), 'stagiaire', '2015-08-14 00:00:00');
INSERT INTO `epresence`.`utilisateur` (`id`, `nom`, `prenom`, `mail`, `motdepasse`, `role`, `dateInscription`) VALUES ('1014', 'Guery', 'David', 'david_guery@hotmail.com', AES_ENCRYPT('pwd15', 'MySecret'), 'stagiaire', '2015-08-15 00:00:00');
INSERT INTO `epresence`.`utilisateur` (`id`, `nom`, `prenom`, `mail`, `motdepasse`, `role`, `dateInscription`) VALUES ('1015', 'Zinoune', 'Hatim', 'hatim_zinoune@outlook.fr', AES_ENCRYPT('pwd16', 'MySecret'), 'stagiaire', '2015-09-01 00:00:00');
INSERT INTO `epresence`.`utilisateur` (`id`, `nom`, `prenom`, `mail`, `motdepasse`, `role`, `dateInscription`) VALUES ('1016', 'Tandjigora', 'Diénaba', 'dienaba.tandjigora@gmail.com', AES_ENCRYPT('pwd17', 'MySecret'), 'stagiaire', '2015-08-20 00:00:00');
INSERT INTO `epresence`.`utilisateur` (`id`, `nom`, `prenom`, `mail`, `motdepasse`, `role`, `dateInscription`) VALUES ('1017', 'Bakhma', 'Amina', 'mina.bakhma@gmail.com', AES_ENCRYPT('pwd18', 'MySecret'), 'stagiaire', '2015-08-06 00:00:00');
INSERT INTO `epresence`.`utilisateur` (`id`, `nom`, `prenom`, `mail`, `motdepasse`, `role`, `dateInscription`) VALUES ('1018', 'Idjihadi', 'Annima', 'idjihadi.a@gmail.com', AES_ENCRYPT('pwd19', 'MySecret'), 'stagiaire', '2015-08-25 00:00:00');

#cours
INSERT INTO `epresence`.`cours` (`id`, `intitule`, `dateDebut`, `dateFin`) VALUES ('200', 'Java', '2015-10-01', '2016-12-18');
INSERT INTO `epresence`.`cours` (`id`, `intitule`, `dateDebut`, `dateFin`) VALUES ('201', 'J2EE', '2015-10-01', '2015-11-05');
INSERT INTO `epresence`.`cours` (`id`, `intitule`, `dateDebut`, `dateFin`) VALUES ('202', 'Base de données', '2015-10-15', '2015-10-22');
INSERT INTO `epresence`.`cours` (`id`, `intitule`, `dateDebut`, `dateFin`) VALUES ('203', 'Plan de Test', '2015-11-17', '2015-10-18');
INSERT INTO `epresence`.`cours` (`id`, `intitule`, `dateDebut`, `dateFin`) VALUES ('200', 'HTML+CSS', '2015-10-26', '2015-10-30');
INSERT INTO `epresence`.`cours` (`id`, `intitule`, `dateDebut`, `dateFin`) VALUES ('205', 'Javascript', '2015-10-26', '2015-10-28');
INSERT INTO `epresence`.`cours` (`id`, `intitule`, `dateDebut`, `dateFin`) VALUES ('206', 'AngularJs', '2015-11-02', '2015-11-07');
INSERT INTO `epresence`.`cours` (`id`, `intitule`, `dateDebut`, `dateFin`) VALUES ('207', 'Gestion de projet', '2015-11-09', '2015-11-14');
INSERT INTO `epresence`.`cours` (`id`, `intitule`, `dateDebut`, `dateFin`) VALUES ('208', 'Gestiobn de projet', '2015-11-16', '2015-11-19');

#Calendrier
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1013', '2016-03-21', 'null', '0', '0000-00-00 00:00:00', '00.0.000.00');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1014', '2016-03-21', 'null', '0', '0000-00-00 00:00:00', '00.0.000.00');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1015', '2016-03-21', 'null', '0', '0000-00-00 00:00:00', '00.0.000.00');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1016', '2016-03-21', 'null', '0', '0000-00-00 00:00:00', '00.0.000.00');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1017', '2016-03-21', 'null', '0', '0000-00-00 00:00:00', '00.0.000.00');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1018', '2016-03-21', 'null', '0', '0000-00-00 00:00:00', '00.0.000.00');

INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1013', '2016-03-22', 'null', '0', '0000-00-00 00:00:00', '00.0.000.00');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1014', '2016-03-22', 'null', '0', '0000-00-00 00:00:00', '00.0.000.00');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1015', '2016-03-22', 'null', '0', '0000-00-00 00:00:00', '00.0.000.00');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1016', '2016-03-22', 'null', '0', '0000-00-00 00:00:00', '00.0.000.00');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1017', '2016-03-22', 'null', '0', '0000-00-00 00:00:00', '00.0.000.00');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1018', '2016-03-22', 'null', '0', '0000-00-00 00:00:00', '00.0.000.00');

INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1013', '2016-03-23', 'null', '0', '0000-00-00 00:00:00', '00.0.000.00');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1014', '2016-03-23', 'null', '0', '0000-00-00 00:00:00', '00.0.000.00');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1015', '2016-03-23', 'null', '0', '0000-00-00 00:00:00', '00.0.000.00');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1016', '2016-03-23', 'null', '0', '0000-00-00 00:00:00', '00.0.000.00');
INSERT INTO `presence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1017', '2016-03-23', 'null', '0', '0000-00-00 00:00:00', '00.0.000.00');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1018', '2016-03-23', 'null', '0', '0000-00-00 00:00:00', '00.0.000.00');

INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1013', '2016-03-24', 'null', '0', '0000-00-00 00:00:00', '00.0.000.00');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1014', '2016-03-24', 'null', '0', '0000-00-00 00:00:00', '00.0.000.00');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1015', '2016-03-24', 'null', '0', '0000-00-00 00:00:00', '00.0.000.00');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1016', '2016-03-24', 'null', '0', '0000-00-00 00:00:00', '00.0.000.00');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1017', '2016-03-24', 'null', '0', '0000-00-00 00:00:00', '00.0.000.00');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1018', '2016-03-24', 'null', '0', '0000-00-00 00:00:00', '00.0.000.00');

INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1013', '2016-03-25', 'null', '0', '0000-00-00 00:00:00', '00.0.000.00');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1014', '2016-03-25', 'null', '0', '0000-00-00 00:00:00', '00.0.000.00');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1015', '2016-03-25', 'null', '0', '0000-00-00 00:00:00', '00.0.000.00');
INSERT INTO `presence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1016', '2016-03-25', 'null', '0', '0000-00-00 00:00:00', '00.0.000.00');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1017', '2016-03-25', 'null', '0', '0000-00-00 00:00:00', '00.0.000.00');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1018', '2016-03-25', 'null', '0', '0000-00-00 00:00:00', '00.0.000.00');








INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1002', '2015-10-01', 'JAVA1', '0', '2015-10-01 08:10:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1005', '2015-10-01', 'JAVA1', '0', '2015-10-01 08:10:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1006', '2015-10-01', 'JAVA1', '0', '2015-10-01 08:12:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1007', '2015-10-01', 'JAVA1', '0', '2015-10-01 08:10:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1008', '2015-10-01', 'JAVA1', '0', '2015-10-01 08:15:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1009', '2015-10-01', 'JAVA1', '1', '2015-10-01 08:10:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('201', '1002', '2015-10-15', 'JEE1', '0', '2015-10-15 08:02:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('201', '1005', '2015-10-15', 'JEE1', '0', '2015-10-15 08:10:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('201', '1006', '2015-10-15', 'JEE1', '0', '2015-10-15 14:12:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('201', '1007', '2015-10-15', 'JEE1', '0', '2015-10-15 19:10:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('201', '1008', '2015-10-15', 'JEE1', '0', '2015-10-15 12:15:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('201', '1009', '2015-10-15', 'JEE1', '0', '2015-10-15 18:10:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('203', '1005', '2015-10-21', 'BD1', '0', '2015-10-15 13:10:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('203', '1006', '2015-10-21', 'BD1', '1', '2015-10-15 14:12:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('203', '1007', '2015-10-21', 'BD1', '1', '2015-10-15 15:10:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('203', '1008', '2015-10-21', 'BD1', '1', '2015-10-15 16:15:00', '10.3.110.60');
INSERT INTO `presence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('203', '1009', '2015-10-21', 'BD1', '0', '2015-10-15 18:10:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1003', '2015-10-21', 'HTML1', '0', '2015-10-26 13:02:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1005', '2015-10-21', 'HTML1', '0', '2015-10-26 13:10:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1006', '2015-10-21', 'HTML1', '0', '2015-10-26 14:12:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1007', '2015-10-21', 'HTML1', '1', '2015-10-26 15:10:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1008', '2015-10-21', 'HTML1', '1', '2015-10-26 15:00:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('200', '1009', '2015-10-21', 'HTML1', '0', '2015-10-26 18:10:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('205', '1008', '2015-10-28', 'JAVAS1', '0', '2015-10-28 13:02:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('205', '1009', '2015-10-28', 'JAVAS1', '0', '2015-10-28 13:10:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('205', '1010', '2015-10-28', 'JAVAS1', '0', '2015-10-28 14:12:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('205', '1011', '2015-10-28', 'JAVAS1', '0', '2015-10-28 15:10:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('205', '1012', '2015-10-28', 'JAVAS1', '0', '2015-10-28 16:15:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('205', '1013', '2015-10-28', 'JAVAS1', '0', '2015-10-28 18:10:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('206', '1004', '2015-11-02', 'ANG1', '0', '2015-11-02 13:02:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('206', '1009', '2015-11-02', 'ANG1', '0', '2015-11-02 16:10:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('206', '1010', '2015-11-02', 'ANG1', '0', '2015-11-02 16:10:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('206', '1011', '2015-11-02', 'ANG1', '0', '2015-11-02 16:10:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('206', '1012', '2015-11-02', 'ANG1', '0', '2015-11-02 16:15:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('206', '1013', '2015-11-02', 'ANG1', '0', '2015-11-02 17:10:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('206', '1014', '2015-11-02', 'ANG1', '0', '2015-11-02 16:10:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('206', '1015', '2015-11-02', 'ANG1', '0', '2015-11-02 16:10:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('206', '1016', '2015-11-02', 'ANG1', '0', '2015-11-02 16:10:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('206', '1017', '2015-11-02', 'ANG1', '1', '2015-11-02 16:15:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('206', '1018', '2015-11-02', 'ANG1', '1', '2015-11-02 17:10:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('207', '1004', '2015-11-09', 'GP1', '0', '2015-11-09 08:02:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('207', '1009', '2015-11-09', 'GP1', '0', '2015-11-09 16:10:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('207', '1010', '2015-11-09', 'GP1', '0', '2015-11-09 16:10:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('207', '1011', '2015-11-09', 'GP1', '0', '2015-11-09 12:10:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('207', '1012', '2015-11-09', 'GP1', '0', '2015-11-09 16:15:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('207', '1013', '2015-11-09', 'GP1', '0', '2015-11-09 17:10:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('207', '1014', '2015-11-09', 'GP1', '1', '2015-11-09 15:10:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('207', '1015', '2015-11-09', 'GP1', '0', '2015-11-09 16:10:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('207', '1016', '2015-11-09', 'GP1', '0', '2015-11-09 16:10:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('207', '1017', '2015-11-09', 'GP1', '0', '2015-11-09 16:15:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('207', '1018', '2015-11-09', 'GP1', '0', '2015-11-09 17:10:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('208', '1004', '2015-11-17', 'GS1', '0', '2015-11-17 08:02:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('208', '1009', '2015-11-17', 'GS1', '0', '2015-11-17 16:10:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('208', '1010', '2015-11-17', 'GS1', '1', '2015-11-17 16:10:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('208', '1011', '2015-11-17', 'GS1', '1', '2015-11-17 12:10:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('208', '1012', '2015-11-17', 'GS1', '0', '2015-11-17 16:15:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('208', '1013', '2015-11-17', 'GS1', '0', '2015-11-17 17:10:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('208', '1014', '2015-11-17', 'GS1', '1', '2015-11-17 15:10:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('208', '1015', '2015-11-17', 'GS1', '0', '2015-11-17 16:10:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('208', '1016', '2015-11-17', 'GS1', '1', '2015-11-17 16:10:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('208', '1017', '2015-11-17', 'GS1', '1', '2015-11-17 16:15:00', '10.3.110.60');
INSERT INTO `epresence`.`calendrier` (`Cours_id`, `Utilisateur_id`, `date`, `cleSignature`, `absent`, `dateSignature`, `ipSignature`) VALUES  ('208', '1018', '2015-11-17', 'GS1', '0', '2015-11-17 17:10:00', '10.3.110.51');


#Quries
SELECT * FROM epresence.calendrier;

SELECT calendrier.cours_id, calendrier.utilisateur_id FROM epresence.calendrier WHERE calendrier.dateSignature > '2015-11-01';
 
SELECT calendrier.utilisateur_id FROM epresence.calendrier WHERE calendrier.cours_id='207' and absent='0';

SELECT adresseip.numero FROM epresence.adresseip WHERE id>20;


grant all privileges on *.* to root@10.3.110.69 identified by "root";
 
flush privileges;
 
grant all privileges on *.* to root@10.3.110.60 identified by "root";
 
flush privileges;

grant all privileges on *.* to root@10.3.110.53 identified by "root";
 
flush privileges;
