package fr.test;

import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import fr.bd.AccesBdCalendrier;
import fr.bd.AccesBdUtilisateur;
import fr.bd.CalendrierLecture;
import fr.presence.IAcces;

public class TestAccesBdUtilisateur {
	public static final String DB_URL = "jdbc:mysql://10.3.110.60/epresencetest?useSSL=false"; // adresse IP du PC de amina

	private AccesBdUtilisateur abdUtil = null;
	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

	@Before
	public void connecter() {
		try {
			this.abdUtil = new AccesBdUtilisateur(IAcces.DB_DRIVER);
			this.abdUtil.seConnecter(DB_URL, IAcces.DB_LOGIN, IAcces.DB_PWD);
		} catch (SQLException e) {
			e.printStackTrace();
			Assert.fail("Connexion à la base de données impossible ! (" + e.getMessage() + ")");
		}
	}

	@After
	public void deconnecter() {
		Assert.assertNotNull("La connexion n'a pas été initialisée.", abdUtil);
		this.abdUtil.seDeconnecter();
	}

	@Test
	public void testAuthentifierOK() {
		Integer status = null;
		try {
			status = Integer.valueOf(abdUtil.authentifier("hatim_zinoune@outlook.fr", "pwd16"));
		} catch (SQLException e) {
			e.printStackTrace();
			Assert.fail("Connexion à la base de données impossible! (" + e.getMessage() + ")");
		}
		Assert.assertNotNull("l'id de l'utilisateur n'a pas été récupéré", status);
		Assert.assertEquals("l'identification n'a pas réussi", 1015, status.intValue());
	}

	@Test
	public void testAuthentifierKoLogin() {
		Integer status = null;
		try {
			status = Integer.valueOf(abdUtil.authentifier("uyhbvfdlshj", "pwd16"));
		} catch (SQLException e) {
			e.printStackTrace();
			Assert.fail("Connexion à la base de données impossible! (" + e.getMessage() + ")");
		}
		Assert.assertNotNull("l'id de l'utilisateur n'a pas été récupéré", status);
		Assert.assertEquals("l'identification n'a pas réussi", -1, status.intValue());
	}

	@Test
	public void testAuthentifierKoMdp() {
		Integer status = null;
		try {
			status = Integer.valueOf(abdUtil.authentifier("hatim_zinoune@outlook.fr", "iubdfshkb"));
		} catch (SQLException e) {
			e.printStackTrace();
			Assert.fail("Connexion à la base de données impossible! (" + e.getMessage() + ")");
		}
		Assert.assertNotNull("l'id de l'utilisateur n'a pas été récupéré", status);
		Assert.assertEquals("l'identification n'a pas réussi", -2, status.intValue());
	}

	@Test
	public void testGenererCode() {
		String code = null;
		AccesBdCalendrier abdCal = null;
		List<CalendrierLecture> listCal = null;
		try {
			code = abdUtil.genererCode(1002, 200);
			abdCal = new AccesBdCalendrier(IAcces.DB_DRIVER);
			abdCal.seConnecter(DB_URL, IAcces.DB_LOGIN, IAcces.DB_PWD);
			Date dateCours = new Date(System.currentTimeMillis());
			listCal = abdCal.selectCalendrier(Integer.valueOf(200), null, dateCours);
		} catch (SQLException e) {
			e.printStackTrace();
			Assert.fail("Connexion avec la base de donnés impossible! (" + e.getMessage() + ")");
		} finally {
			Assert.assertNotNull("la connexion à Calendrier n'a pas été initialisé", abdCal);
			abdCal.seDeconnecter();
		}
		Assert.assertNotNull("La list de calendrier n'a pas été récupérée", listCal);
		String codeMod = listCal.get(0).getCleSignature();

		Assert.assertNotNull("le code n'a pas été généré", code);
		Assert.assertEquals("le code n'a pas été enregistrer dans la base", codeMod, code);
	}

	@Test
	public void testSigner() {
		Integer status = null;
		try {
			status = abdUtil.signer(Integer.valueOf(200), Integer.valueOf(1002),
					new Date(sdf.parse("16/03/2016").getTime()), "H15p8iFV", "10.3.110.70");
		} catch (ParseException e) {
			e.printStackTrace();
			Assert.fail("la date n'est pas au bon format");
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Connexion à la base impossible! (" + e.getMessage() + ")");
		}
		Assert.assertNotNull("le status n'a pas été attribué", status);
		Assert.assertEquals("la signature a échoué", 1, status.intValue());
	}
}
