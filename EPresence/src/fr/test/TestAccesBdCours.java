package fr.test;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import fr.bd.AccesBdCours;
import fr.bd.StagiaireAbs;
import fr.presence.Cours;
import fr.presence.IAcces;

/**
 * Class de test de la méthode AccesBdCours.
 * 
 * @author David Guery
 *
 */
public class TestAccesBdCours {

	public static final String DB_URL = "jdbc:mysql://10.3.110.60/epresencetest?useSSL=false"; // adresse IP du PC de amina

	private AccesBdCours abdCours = null;
	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

	@Before
	public void connecter() {
		try {
			this.abdCours = new AccesBdCours(IAcces.DB_DRIVER);
			this.abdCours.seConnecter(DB_URL, IAcces.DB_LOGIN, IAcces.DB_PWD);
		} catch (SQLException e) {
			e.printStackTrace();
			Assert.fail("Connexion à la base de données impossible ! (" + e.getMessage() + ")");
		}
	}

	@After
	public void deconnecter() {
		Assert.assertNotNull("La connexion n'a pas été initialisée.", abdCours);
		this.abdCours.seDeconnecter();
	}

	@Test
	public void testSelectAllCours() {
		List<Cours> listeCours = null;
		try {
			listeCours = abdCours.selectAllCours();
		} catch (SQLException e) {
			e.printStackTrace();
			Assert.fail("la connexion avec la base de données est impossible! (" + e.getMessage() + ")");
		}
		Assert.assertNotNull("la liste de cours n'a pas été récupérée", listeCours);
		Assert.assertFalse("la liste de cours est vide", listeCours.isEmpty());
		Assert.assertTrue("le nombre de cours retrouvé n'est pas conforme à celui attendu", listeCours.size() == 9);
	}

	@Test
	public void testSelectCours() {
		Integer idUtilisateur = Integer.valueOf(1002);
		List<Cours> listeCours = null;
		try {
			listeCours = abdCours.selectCours(idUtilisateur);
		} catch (SQLException e) {
			e.printStackTrace();
			Assert.fail("Connecion à la base de données impossible! (" + e.getMessage() + ")");
		}
		Assert.assertNotNull("Le cours n'a pas été récupéré.", listeCours);
		Assert.assertFalse("la liste de cours de l'utilisateur est vide", listeCours.isEmpty());
		Assert.assertEquals("Le cours récupéré ne correspond pas à celui attendu", 5, listeCours.size());
	}

	@Test
	public void testNbPresentAbsentUnCourUnJour() {
		Integer nbPresent = null;
		Integer nbAbsent = null;
		try {
			abdCours.seConnecter(DB_URL, IAcces.DB_LOGIN, IAcces.DB_PWD);
			java.sql.Date dateCours = new java.sql.Date(sdf.parse("11/03/2016").getTime());
			nbPresent = abdCours.nbPresentAbsent(Integer.valueOf(200), Boolean.FALSE, dateCours, dateCours);
			nbAbsent = abdCours.nbPresentAbsent(Integer.valueOf(200), Boolean.TRUE, dateCours, dateCours);
		} catch (SQLException e) {
			e.printStackTrace();
			Assert.fail("Connexion à la base de données impossible! (" + e.getMessage() + ")");
		} catch (ParseException e) {
			e.printStackTrace();
			Assert.fail("Le format de la date n'est pas correcte. (" + e.getMessage() + ")");
		}
		Assert.assertNotNull("Le nombre de présent n'a pas été trouvé", nbPresent);
		Assert.assertNotNull("Le nombre d'absent n'a pas été trouvé", nbAbsent);
		Assert.assertEquals("Le nombre de présent n'est pas celui attendu", 5, nbPresent.intValue());
		Assert.assertEquals("Le nombre d'absent n'est pas celui attendu", 1, nbAbsent.intValue());
	}

	@Test
	public void testNbPresentAbsentUnCoursPeriode() {
		Integer nbPresent = null;
		Integer nbAbsent = null;
		try {
			abdCours.seConnecter(DB_URL, IAcces.DB_LOGIN, IAcces.DB_PWD);
			java.sql.Date dateDebut = new java.sql.Date(sdf.parse("10/03/2016").getTime());
			java.sql.Date dateFin = new java.sql.Date(sdf.parse("16/03/2016").getTime());
			nbPresent = abdCours.nbPresentAbsent(Integer.valueOf(200), Boolean.FALSE, dateDebut, dateFin);
			nbAbsent = abdCours.nbPresentAbsent(Integer.valueOf(200), Boolean.TRUE, dateDebut, dateFin);
		} catch (SQLException e) {
			e.printStackTrace();
			Assert.fail("Connexion à la base de données impossible! (" + e.getMessage() + ")");
		} catch (ParseException e) {
			e.printStackTrace();
			Assert.fail("Le format de la date n'est pas correcte. (" + e.getMessage() + ")");
		}
		Assert.assertNotNull("Le nombre de présent n'a pas été trouvé", nbPresent);
		Assert.assertNotNull("Le nombre d'absent n'a pas été trouvé", nbAbsent);
		Assert.assertEquals("Le nombre de présent n'est pas celui attendu", 10, nbPresent.intValue());
		Assert.assertEquals("Le nombre d'absent n'est pas celui attendu", 1, nbAbsent.intValue());
	}

	@Test
	public void testNbPresentAbsentPeriode() {
		Integer nbPresent = null;
		Integer nbAbsent = null;
		try {
			abdCours.seConnecter(DB_URL, IAcces.DB_LOGIN, IAcces.DB_PWD);
			java.sql.Date dateDebut = new java.sql.Date(sdf.parse("10/03/2016").getTime());
			java.sql.Date dateFin = new java.sql.Date(sdf.parse("16/03/2016").getTime());
			nbPresent = abdCours.nbPresentAbsent(null, Boolean.FALSE, dateDebut, dateFin);
			nbAbsent = abdCours.nbPresentAbsent(null, Boolean.TRUE, dateDebut, dateFin);
		} catch (SQLException e) {
			e.printStackTrace();
			Assert.fail("Connexion à la base de données impossible! (" + e.getMessage() + ")");
		} catch (ParseException e) {
			e.printStackTrace();
			Assert.fail("Le format de la date n'est pas correcte. (" + e.getMessage() + ")");
		}
		Assert.assertNotNull("Le nombre de présent n'a pas été trouvé", nbPresent);
		Assert.assertNotNull("Le nombre d'absent n'a pas été trouvé", nbAbsent);
		Assert.assertEquals("Le nombre de présent n'est pas celui attendu", 11, nbPresent.intValue());
		Assert.assertEquals("Le nombre d'absent n'est pas celui attendu", 2, nbAbsent.intValue());
	}

	@Test
	public void testNbPresentAbsentUnCours() {
		Integer nbPresent = null;
		Integer nbAbsent = null;
		try {
			abdCours.seConnecter(DB_URL, IAcces.DB_LOGIN, IAcces.DB_PWD);
			nbPresent = abdCours.nbPresentAbsent(Integer.valueOf(200), Boolean.FALSE, null, null);
			nbAbsent = abdCours.nbPresentAbsent(Integer.valueOf(200), Boolean.TRUE, null, null);
		} catch (SQLException e) {
			e.printStackTrace();
			Assert.fail("Connexion à la base de données impossible! (" + e.getMessage() + ")");
		}
		Assert.assertNotNull("Le nombre de présent n'a pas été trouvé", nbPresent);
		Assert.assertNotNull("Le nombre d'absent n'a pas été trouvé", nbAbsent);
		Assert.assertEquals("Le nombre de présent n'est pas celui attendu", 10, nbPresent.intValue());
		Assert.assertEquals("Le nombre d'absent n'est pas celui attendu", 1, nbAbsent.intValue());
	}

	@Test
	public void testNbPresentAbsentTotal() {
		Integer nbPresent = null;
		Integer nbAbsent = null;
		try {
			abdCours.seConnecter(DB_URL, IAcces.DB_LOGIN, IAcces.DB_PWD);
			nbPresent = abdCours.nbPresentAbsent(null, Boolean.FALSE, null, null);
			nbAbsent = abdCours.nbPresentAbsent(null, Boolean.TRUE, null, null);
		} catch (SQLException e) {
			e.printStackTrace();
			Assert.fail("Connexion à la base de données impossible! (" + e.getMessage() + ")");
		}
		Assert.assertNotNull("Le nombre de présent n'a pas été trouvé", nbPresent);
		Assert.assertNotNull("Le nombre d'absent n'a pas été trouvé", nbAbsent);
		Assert.assertEquals("Le nombre de présent n'est pas celui attendu", 53, nbPresent.intValue());
		Assert.assertEquals("Le nombre d'absent n'est pas celui attendu", 15, nbAbsent.intValue());
	}

	@Test
	public void testListPresentAbsentUnCourUnJour() {
		List<StagiaireAbs> listAbs = null;
		List<StagiaireAbs> listPres = null;
		try {
			abdCours.seConnecter(DB_URL, IAcces.DB_LOGIN, IAcces.DB_PWD);
			java.sql.Date dateCours = new java.sql.Date(sdf.parse("11/03/2016").getTime());
			listPres = abdCours.listePresentAbsent(Integer.valueOf(200), Boolean.FALSE, dateCours, dateCours);
			listAbs = abdCours.listePresentAbsent(Integer.valueOf(200), Boolean.TRUE, dateCours, dateCours);
		} catch (SQLException e) {
			e.printStackTrace();
			Assert.fail("Connexion à la base de données impossible! (" + e.getMessage() + ")");
		} catch (ParseException e) {
			e.printStackTrace();
			Assert.fail("Le format de la date n'est pas correcte. (" + e.getMessage() + ")");
		}
		Assert.assertNotNull("Le nombre de présent n'a pas été trouvé", listAbs);
		Assert.assertNotNull("Le nombre d'absent n'a pas été trouvé", listPres);
		Assert.assertFalse("la liste d'absent récupérée est vide", listAbs.isEmpty());
		Assert.assertFalse("la liste de present récupérée est vide", listPres.isEmpty());
		Assert.assertEquals("Le nombre de présent n'est pas celui attendu", 4, listPres.size());
		Assert.assertEquals("Le nombre d'absent n'est pas celui attendu", 1, listAbs.size());
	}

	@Test
	public void testListPresentAbsentUnCourPeriode() {
		List<StagiaireAbs> listAbs = null;
		List<StagiaireAbs> listPres = null;
		try {
			abdCours.seConnecter(DB_URL, IAcces.DB_LOGIN, IAcces.DB_PWD);
			java.sql.Date dateDebut = new java.sql.Date(sdf.parse("10/03/2016").getTime());
			java.sql.Date dateFin = new java.sql.Date(sdf.parse("16/03/2016").getTime());
			listPres = abdCours.listePresentAbsent(Integer.valueOf(200), Boolean.FALSE, dateDebut, dateFin);
			listAbs = abdCours.listePresentAbsent(Integer.valueOf(200), Boolean.TRUE, dateDebut, dateFin);
		} catch (SQLException e) {
			e.printStackTrace();
			Assert.fail("Connexion à la base de données impossible! (" + e.getMessage() + ")");
		} catch (ParseException e) {
			e.printStackTrace();
			Assert.fail("Le format de la date n'est pas correcte. (" + e.getMessage() + ")");
		}
		Assert.assertNotNull("Le nombre de présent n'a pas été trouvé", listAbs);
		Assert.assertNotNull("Le nombre d'absent n'a pas été trouvé", listPres);
		Assert.assertFalse("la liste récupéré est vide", listAbs.isEmpty());
		Assert.assertFalse("la liste récupéré est vide", listPres.isEmpty());
		Assert.assertEquals("Le nombre de présent n'est pas celui attendu", 7, listPres.size());
		Assert.assertEquals("Le nombre d'absent n'est pas celui attendu", 1, listAbs.size());
	}

	@Test
	public void testListPresentAbsentPeriode() {
		List<StagiaireAbs> listAbs = null;
		List<StagiaireAbs> listPres = null;
		try {
			abdCours.seConnecter(DB_URL, IAcces.DB_LOGIN, IAcces.DB_PWD);
			java.sql.Date dateDebut = new java.sql.Date(sdf.parse("10/03/2016").getTime());
			java.sql.Date dateFin = new java.sql.Date(sdf.parse("16/03/2016").getTime());
			listPres = abdCours.listePresentAbsent(null, Boolean.FALSE, dateDebut, dateFin);
			listAbs = abdCours.listePresentAbsent(null, Boolean.TRUE, dateDebut, dateFin);
		} catch (SQLException e) {
			e.printStackTrace();
			Assert.fail("Connexion à la base de données impossible! (" + e.getMessage() + ")");
		} catch (ParseException e) {
			e.printStackTrace();
			Assert.fail("Le format de la date n'est pas correcte. (" + e.getMessage() + ")");
		}
		Assert.assertNotNull("Le nombre de présent n'a pas été trouvé", listAbs);
		Assert.assertNotNull("Le nombre d'absent n'a pas été trouvé", listPres);
		Assert.assertFalse("la liste récupéré est vide", listAbs.isEmpty());
		Assert.assertFalse("la liste récupéré est vide", listPres.isEmpty());
		Assert.assertEquals("Le nombre de présent n'est pas celui attendu", 7, listPres.size());
		Assert.assertEquals("Le nombre d'absent n'est pas celui attendu", 1, listAbs.size());
	}

	@Test
	public void testListPresentAbsentUnCour() {
		List<StagiaireAbs> listAbs = null;
		List<StagiaireAbs> listPres = null;
		try {
			abdCours.seConnecter(DB_URL, IAcces.DB_LOGIN, IAcces.DB_PWD);
			listPres = abdCours.listePresentAbsent(Integer.valueOf(200), Boolean.FALSE, null, null);
			listAbs = abdCours.listePresentAbsent(Integer.valueOf(200), Boolean.TRUE, null, null);
		} catch (SQLException e) {
			e.printStackTrace();
			Assert.fail("Connexion à la base de données impossible! (" + e.getMessage() + ")");
		}
		Assert.assertNotNull("Le nombre de présent n'a pas été trouvé", listAbs);
		Assert.assertNotNull("Le nombre d'absent n'a pas été trouvé", listPres);
		Assert.assertFalse("la liste récupéré est vide", listAbs.isEmpty());
		Assert.assertFalse("la liste récupéré est vide", listPres.isEmpty());
		Assert.assertEquals("Le nombre de présent n'est pas celui attendu", 7, listPres.size());
		Assert.assertEquals("Le nombre d'absent n'est pas celui attendu", 1, listAbs.size());
	}

	@Test
	public void testListPresentAbsent() {
		List<StagiaireAbs> listAbs = null;
		List<StagiaireAbs> listPres = null;
		try {
			abdCours.seConnecter(DB_URL, IAcces.DB_LOGIN, IAcces.DB_PWD);
			listPres = abdCours.listePresentAbsent(null, Boolean.FALSE, null, null);
			listAbs = abdCours.listePresentAbsent(null, Boolean.TRUE, null, null);
		} catch (SQLException e) {
			e.printStackTrace();
			Assert.fail("Connexion à la base de données impossible! (" + e.getMessage() + ")");
		}
		Assert.assertNotNull("Le nombre de présent n'a pas été trouvé", listAbs);
		Assert.assertNotNull("Le nombre d'absent n'a pas été trouvé", listPres);
		Assert.assertFalse("la liste récupéré est vide", listAbs.isEmpty());
		Assert.assertFalse("la liste récupéré est vide", listPres.isEmpty());
		Assert.assertEquals("Le nombre de présent n'est pas celui attendu", 45, listPres.size());
		Assert.assertEquals("Le nombre d'absent n'est pas celui attendu", 14, listAbs.size());
	}
}
