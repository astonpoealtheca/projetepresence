package fr;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.mail.MessagingException;

import fr.presence.Cours;
import fr.presence.EpresenceException;
import fr.presence.Formateur;
import fr.presence.Stagiaire;
import fr.rest.SendEMail;

public class Run {

	public static void main(String[] args) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date debut = null;
		Date fin = null;
		try {
			debut = sdf.parse("12/03/2016");
			fin = sdf.parse("12/04/2016");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Cours monCours = null;
		if (debut != null && fin != null) {
			monCours = new Cours("angularJS", 1, debut, fin);
			monCours.genererCalendrier();
			System.out.println(monCours);
			System.out.println(monCours.getCalendriers());
		}

		Date dateInsStag = null;
		Date dateInsForm = null;
		try {
			dateInsStag = sdf.parse("01/09/2014");
			dateInsForm = sdf.parse("01/08/2012");
		} catch (ParseException e1) {
			e1.printStackTrace();
		}

		Stagiaire s1 = new Stagiaire("Guery", "David", "david_guery@hotmail.com", Integer.valueOf(1), "mdp",
				dateInsStag);
		System.out.println(s1);
		Formateur f1 = new Formateur("Zinoune", "Hatim", "hatim.zinoune@sqli.com", Integer.valueOf(2), "mdp",
				dateInsForm);
		System.out.println(f1);

		if (monCours != null) {
			s1.ajouterCours(monCours);
			System.out.println(s1.getTabCours());
			f1.ajouterCours(monCours);
			System.out.println(f1.getTabCours());
			try {
				String cleSign = f1.genererCleSignature(1);
				System.out.println(cleSign);
				System.out.println(s1.signer(cleSign, 1));
				System.out.println(s1.getTabCours().get(Integer.valueOf(1)).getCalendriers());
				System.out.println(f1.getTabCours().get(Integer.valueOf(1)).getCalendriers());
			} catch (EpresenceException e) {
				e.getMessage();
				e.printStackTrace();
			}
		}

		/** test de la méthode AccesBdCours.selectAllCours */
		// AccesBdCours abdCours = null;
		// List<Cours> listCours = new ArrayList<>();
		// try {
		// abdCours = new AccesBdCours(IAcces.DB_DRIVER);
		// abdCours.seConnecter(IAcces.DB_URL, IAcces.DB_LOGIN, IAcces.DB_PWD);
		// listCours = abdCours.selectAllCours();
		// } catch (SQLException e) {
		// e.printStackTrace();
		// } finally {
		// if (abdCours != null) {
		// abdCours.seDeconnecter();
		// }
		// }

		/** test de la méthode AccesBdCalendrier.selectCalendrier */
		// List<Calendrier> listCal1 = null;
		// List<Calendrier> listCal2 = null;
		// List<Calendrier> listCal3 = null;
		// List<Calendrier> listCal4 = null;
		// AccesBdCalendrier abdCal = null;
		// if (listCours != null) {
		// System.out.println(listCours);
		//
		// try {
		// abdCal = new AccesBdCalendrier(IAcces.DB_DRIVER);
		// abdCal.seConnecter(IAcces.DB_URL, IAcces.DB_LOGIN, IAcces.DB_PWD);
		// listCal1 =
		// abdCal.selectCalendrier(Integer.valueOf(listCours.get(0).getId()),
		// null, null);
		// listCal2 = abdCal.selectCalendrier(null, null, null);
		// listCal3 = abdCal.selectCalendrier(null, Integer.valueOf(1002),
		// null);
		// Date jour = sdf.parse("11/03/2016");
		// java.sql.Date dateJour = new java.sql.Date(jour.getTime());
		// listCal4 = abdCal.selectCalendrier(Integer.valueOf(200),
		// Integer.valueOf(1009), dateJour);
		// } catch (SQLException e) {
		// e.printStackTrace();
		// } catch (ParseException e) {
		// e.printStackTrace();
		// } finally {
		// if (abdCal != null) {
		// abdCal.seDeconnecter();
		// }
		//
		// }
		//
		// System.out.println(listCal1);
		// System.out.println(listCal2);
		// System.out.println(listCal3);
		// System.out.println(listCal4);
		// }

		/** test de la méthode AccesBdUtilisateur.signer */
		// Calendrier calend = listCal4.get(0);
		// AccesBdUtilisateur abdUtil = null;
		// java.sql.Date dateCour = null;
		// Integer Res = null;
		// try {
		// abdUtil = new AccesBdUtilisateur(IAcces.DB_DRIVER);
		// abdUtil.seConnecter(IAcces.DB_URL, IAcces.DB_LOGIN, IAcces.DB_PWD);
		// dateCour = new java.sql.Date(calend.getDate().getTime());
		// Res = abdUtil.signer(Integer.valueOf(calend.getCoursId()),
		// Integer.valueOf(calend.getUtilisateurId()),
		// dateCour, "HYu4islo");
		// } catch (SQLException e) {
		// e.printStackTrace();
		// } catch (Exception e) {
		// e.printStackTrace();
		// } finally {
		// if (abdUtil != null) {
		// abdUtil.seDeconnecter();
		// }
		//
		// }
		//
		// System.out.println(Res);
		//
		//
		// try {
		// abdCal.seConnecter(IAcces.DB_URL, IAcces.DB_LOGIN, IAcces.DB_PWD);
		// listCal4 =
		// abdCal.selectCalendrier(Integer.valueOf(calend.getCoursId()),
		// Integer.valueOf(calend.getUtilisateurId()), dateCour);
		// } catch (SQLException e) {
		// e.printStackTrace();
		// } finally {
		// if (abdCal != null) {
		// abdCal.seDeconnecter();
		// }
		// }
		// System.out.println(calend);
		// System.out.println(listCal4);

		/** test de la méthode AccesBdCours.nbPresentAbsent */
		// Integer nbPresent = null;
		// Integer nbAbsent = null;
		// try {
		// abdCours.seConnecter(IAcces.DB_URL, IAcces.DB_LOGIN, IAcces.DB_PWD);
		// java.sql.Date dateCours = new
		// java.sql.Date(sdf.parse("11/03/2016").getTime());
		// nbPresent = abdCours.nbPresentAbsent(Integer.valueOf(200),
		// Boolean.FALSE, dateCours, dateCours);
		// nbAbsent = abdCours.nbPresentAbsent(Integer.valueOf(200),
		// Boolean.TRUE, dateCours, dateCours);
		// } catch (SQLException e) {
		// e.printStackTrace();
		// } catch (ParseException e) {
		// e.printStackTrace();
		// } finally {
		// abdCours.seDeconnecter();
		// }
		//
		// System.out.println("present : " + nbPresent + " et absent : " +
		// nbAbsent);

		/** test de la méthode AccesBdCours.listAbsentPresent */
		// List<StagiaireAbs> listStagiaire = null;
		// try {
		// abdCours.seConnecter(IAcces.DB_URL, IAcces.DB_LOGIN, IAcces.DB_PWD);
		// java.sql.Date dateCours = new
		// java.sql.Date(sdf.parse("15/10/2015").getTime());
		// listStagiaire = abdCours.listePresentAbsent(Integer.valueOf(203),
		// Boolean.FALSE, null, null);
		// } catch (SQLException e) {
		// e.printStackTrace();
		// } catch (ParseException e) {
		// e.printStackTrace();
		// } finally {
		// abdCours.seDeconnecter();
		// }
		//
		// System.out.println(listStagiaire);

		/** test de la méthode AccesAdresseIp.selectAdresseIp */
		// AccesBdAdresseIp abdIp = null;
		// List<String> listIp = null;
		// try {
		// abdIp = new AccesBdAdresseIp(IAcces.DB_DRIVER);
		// abdIp.seConnecter(IAcces.DB_URL, IAcces.DB_LOGIN, IAcces.DB_PWD);
		// listIp = abdIp.selectAdressIp();
		// } catch (SQLException e) {
		// e.printStackTrace();
		// } finally {
		// if (abdIp != null) {
		// abdIp.seDeconnecter();
		// }
		// }
		// System.out.println(listIp);

		/** test de la méthode AccesBdUtilisateur.inscrire */
		// List<Object> status = new ArrayList<>();
		// try {
		// abdUtil.seConnecter(IAcces.DB_URL, IAcces.DB_LOGIN, IAcces.DB_PWD);
		// status = abdUtil.inscrire("Blabla", "Bloblo",
		// "blablo.room@hotmail.com", "administrateur");
		// } catch (SQLException e) {
		// e.printStackTrace();
		// } catch (Exception e) {
		// e.printStackTrace();
		// }
		// abdUtil.seDeconnecter();
		// System.out.println(status);

		/** test de la méthode AccesBdCours.attribuerCours */
		// AccesBdCours abdCours = null;
		// List<Integer> status1 = new ArrayList<>();
		// try {
		// abdCours = new AccesBdCours(IAcces.DB_DRIVER);
		// abdCours.seConnecter(IAcces.DB_URL, IAcces.DB_LOGIN, IAcces.DB_PWD);
		// status1 = abdCours.attribuerCours(Integer.valueOf(1020),
		// Integer.valueOf(200));
		// } catch (SQLException e) {
		// e.printStackTrace();
		// } catch (EpresenceException e) {
		// e.printStackTrace();
		// }
		//
		// abdCours.seDeconnecter();
		// System.out.println(status1);

		/** test de la méthode SendEmail.send */
		SendEMail sendEMail = new SendEMail();
		try {
			sendEMail.send("david_guery@hotmail.com", "BlaBla");
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
