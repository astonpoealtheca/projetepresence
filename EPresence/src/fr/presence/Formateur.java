package fr.presence;

import java.security.SecureRandom;
import java.util.Calendar;
import java.util.Date;

/**
 * Class représentant les formateurs donnant les cours pouvant générer la clé de
 * signature pour les stagiaires. Hérite de Utilisateur.
 * 
 * @author David Guery
 *
 */
public class Formateur extends Utilisateur {
	/**
	 * Chaine de caractères à partir de laquelle est générer la clé de
	 * signature.
	 */
	public static final String charset = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	/**
	 * Générateur aléatoire.
	 */
	public static SecureRandom randomSec = new SecureRandom();

	/**
	 * Constructeur par défaut de la class hériter de Utilisateur.
	 */
	public Formateur() {
	}

	/**
	 * Constructeur permettant d'initialiser tous les attributs. Hériter de
	 * Utilisateur.
	 * 
	 * @param nom
	 * @param prenom
	 * @param mail
	 * @param id
	 * @param mdp
	 */
	public Formateur(String nom, String prenom, String mail, Integer id, String mdp, Date dateInscription) {
		super(nom, prenom, mail, id, mdp, dateInscription);
	}

	/**
	 * Cette méthode configure la clé de signature pour un Cours pour le jour où
	 * elle est utilisée.
	 * 
	 * @param coursId
	 *            numéro d'identifiant du Cours pour lequel on veut générer la
	 *            clé.
	 * @return la cle de signature générée.
	 * @throws EpresenceException
	 *             si le cours n'a pas lieu ce jour.
	 */
	public String genererCleSignature(int coursId) throws EpresenceException {
		// Récupération du Cours coursId dans la liste de Cours du formateur
		Cours monCours = getTabCours().get(Integer.valueOf(coursId));
		// La date d'aujourd'hui
		Date today = new Date(System.currentTimeMillis());
		// génération des calendriers pour la manipulation des dates
		Calendar aujourd = Calendar.getInstance();
		Calendar calCours = Calendar.getInstance();
		aujourd.setTime(today);
		for (Calendrier joursDeCours : monCours.getCalendriers()) {
			calCours.setTime(joursDeCours.getDate());
			if (calCours.get(Calendar.YEAR) == aujourd.get(Calendar.YEAR)
					&& calCours.get(Calendar.DAY_OF_YEAR) == aujourd.get(Calendar.DAY_OF_YEAR)) {
				String cle = randomString(8);
				joursDeCours.setCleSignature(cle);
				return cle;
			}
		}
		throw new EpresenceException("Le cours " + monCours.getIntitule() + " n'a pas lieu aujourd'hui");
	}

	/**
	 * Méthode qui génère une chaine de caratères aléatoire d'une longueur
	 * choisie à partir d'une chaine de caractère contenant tous les caractères
	 * possibles.
	 * 
	 * @param len
	 *            longueur de la chaine de caractères à générer.
	 * @return Une chaine de caractères sélectionner aléatoirement dans charset
	 *         et de longueur len.
	 */
	public String randomString(int len) {
		StringBuilder builder = new StringBuilder(len);
		for (int i = 0; i < len; i++) {
			builder.append(charset.charAt(randomSec.nextInt(charset.length())));
		}
		return builder.toString();
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(super.toString());
		builder.deleteCharAt(super.toString().length() - 1);
		builder.append(", Formateur]");
		return builder.toString();
	}

}
