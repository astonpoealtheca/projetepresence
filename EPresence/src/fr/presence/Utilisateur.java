package fr.presence;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Class utilisateur représentant tous les acteurs utilisant l'application de
 * signature.
 * 
 * @author David Guery
 *
 */
public class Utilisateur {
	/**
	 * Nom de l'utilisateur
	 */
	private String nom;
	/**
	 * Prenom de l'Utilisateur
	 */
	private String prenom;
	/**
	 * adresse mail de l'utilisateur
	 */
	private String mail;
	/**
	 * Identifiant unique de l'utilisateur générer par la base de données
	 */
	private Integer id;
	/**
	 * liste des cours auxquels l'utilisateur est inscrit dans le cas d'un
	 * stagiaire ou d'un formateur.
	 */
	private Map<Integer, Cours> tabCours = new HashMap<>();
	/**
	 * mot de passe de l'utilisateur, crypté par la base de données
	 */
	private String mdp;

	/**
	 * date d'inscription de l'utilisateur.
	 */
	private Date dateInscription;

	/**
	 * Constructeur par défaut de la class
	 */
	public Utilisateur() {
		this("NOM", "Prenom", null, null, null, null);
	}

	/**
	 * Constructeur de la Class permettant d'initialiser tous les attributs
	 * 
	 * @param nom
	 * @param prenom
	 * @param mail
	 * @param id
	 * @param mdp
	 */
	public Utilisateur(String nom, String prenom, String mail, Integer id, String mdp, Date dateInscription) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.mail = mail;
		this.id = id;
		setMdp(mdp);
		setDateInscription(dateInscription);
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Map<Integer, Cours> getTabCours() {
		return tabCours;
	}

	private String getMdp() {
		return mdp;
	}

	private void setMdp(String mdp) {
		this.mdp = mdp;
	}

	public Date getDateInscription() {
		return dateInscription;
	}

	public void setDateInscription(Date dateInscription) {
		this.dateInscription = dateInscription;
	}

	/**
	 * Méthode pour ajouter un cours à la liste de cours de l'utilisateur.
	 * 
	 * @param unCours
	 *            un Cours de l'utilisateur
	 */
	public void ajouterCours(Cours unCours) {
		if (unCours != null) {
			this.tabCours.put(Integer.valueOf(unCours.getId()), unCours);
		}
	}

	/**
	 * Méthode pour retirer un cours de la liste de l'utilisateur.
	 * 
	 * @param id
	 *            identifiant du cours à retirer.
	 */
	public void retirerCours(Integer id) {
		if (tabCours.keySet().contains(id)) {
			tabCours.remove(id);
		}
	}

	/**
	 * Méthode pour modifier le mot de passe d'un utilisateur.
	 * 
	 * @param ancienMdp,
	 *            le mot de passe actuel
	 * @param nouveauMdp,
	 *            le nouveau mot de passe
	 * @param nouveauMdpConfimer,
	 *            la confirmation du nouveau mot de passe
	 * @return -2 si le mot de passe entré n'est pas le mot de passe actuel.
	 * @return -1 si le nouveau mot de passe et sa confirmation sont différent.
	 * @return 1 si le changement de mot de passe à réussi.
	 */
	public int modifierMdp(String ancienMdp, String nouveauMdp, String nouveauMdpConfimer) {
		if (ancienMdp == getMdp()) {
			if (nouveauMdp.equals(nouveauMdpConfimer)) {
				setMdp(nouveauMdpConfimer);
				return 1;
			} else {
				return -1;
			}
		} else {
			return -2;
		}
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Utilisateur [nom=");
		builder.append(nom);
		builder.append(", prenom=");
		builder.append(prenom);
		builder.append(", mail=");
		builder.append(mail);
		builder.append(", id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}

}
