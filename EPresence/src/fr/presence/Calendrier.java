package fr.presence;

import java.util.Date;

/**
 * Class représentant chaque jour ou un cour à lieu pour chaque stagiaire.
 * 
 * @author David Guery
 *
 */
public class Calendrier {
	/**
	 * date du jour du calendrier
	 */
	private Date date;
	/**
	 * cle de signature pour le cours en ce jour
	 */
	private String cleSignature;
	/**
	 * statut d'absence pour chaque stagiaire suivant le cours
	 */
	private Boolean absent;
	/**
	 * date et heure de signature de chaque stagiaire.
	 */
	private Date dateSignature;

	/**
	 * Identifiant du cour correspondant à ce calendrier
	 */
	private int coursId;
	/**
	 * Identifiant de l'utilisateur inscrit à ce cours ce jour du calendrier.
	 */
	private int utilisateurId;

	/**
	 * Constructeur de la class ne prenant pas en compte un utilisateur
	 * 
	 * @param date
	 * @param cleSignature
	 * @param absent
	 * @param dateSignature
	 * @param coursId
	 */
	public Calendrier(Date date, String cleSignature, Boolean absent, Date dateSignature, int coursId) {
		super();
		this.date = date;
		this.cleSignature = cleSignature;
		this.absent = absent;
		this.dateSignature = dateSignature;
		this.coursId = coursId;
	}

	/**
	 * Constructeur de la class prenant en compte tout les attributs
	 * 
	 * @param date
	 * @param cleSignature
	 * @param absent
	 * @param dateSignature
	 * @param coursId
	 * @param utilisateurId
	 */
	public Calendrier(Date date, String cleSignature, Boolean absent, Date dateSignature, int coursId,
			int utilisateurId) {
		super();
		this.date = date;
		this.cleSignature = cleSignature;
		this.absent = absent;
		this.dateSignature = dateSignature;
		this.coursId = coursId;
		this.utilisateurId = utilisateurId;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Calendrier [date=");
		builder.append(date);
		builder.append(", absent=");
		builder.append(absent);
		builder.append(", dateSignature=");
		builder.append(dateSignature);
		builder.append(", coursId=");
		builder.append(coursId);
		builder.append(", utilisateurId=");
		builder.append(utilisateurId);
		builder.append("]");
		return builder.toString();
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getCleSignature() {
		return cleSignature;
	}

	public void setCleSignature(String cleSignature) {
		this.cleSignature = cleSignature;
	}

	public Boolean getAbsent() {
		return absent;
	}

	public void setAbsent(Boolean absent) {
		this.absent = absent;
	}

	public Date getDateSignature() {
		return dateSignature;
	}

	public void setDateSignature(Date dateSignature) {
		this.dateSignature = dateSignature;
	}

	public int getCoursId() {
		return coursId;
	}

	public void setCoursId(int coursId) {
		this.coursId = coursId;
	}

	public int getUtilisateurId() {
		return utilisateurId;
	}

	public void setUtilisateurId(int utilisateurId) {
		this.utilisateurId = utilisateurId;
	}

}
