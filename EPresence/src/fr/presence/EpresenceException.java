package fr.presence;

public class EpresenceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1215773576316000385L;

	public EpresenceException() {
	}

	public EpresenceException(String message) {
		super(message);
	}

	public EpresenceException(Throwable cause) {
		super(cause);
	}

	public EpresenceException(String message, Throwable cause) {
		super(message, cause);
	}

	public EpresenceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
