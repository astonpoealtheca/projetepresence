package fr.presence;

import java.util.Calendar;
import java.util.Date;

/**
 * Class représentant les stagiaires pouvant assister aux cours et signer leur
 * présence. Hérite de Utilisateur.
 * 
 * @author David Guery
 *
 */
public class Stagiaire extends Utilisateur {

	/**
	 * Constructeur par défaut de la class. Hériter de Utilisateur.
	 */
	public Stagiaire() {
		super();
	}

	/**
	 * Constructeur permettant l'initialisation de tous les attributs. Hériter
	 * de Utilisateur.
	 * 
	 * @param nom
	 * @param prenom
	 * @param mail
	 * @param id
	 * @param mdp
	 */
	public Stagiaire(String nom, String prenom, String mail, Integer id, String mdp, Date dateInscription) {
		super(nom, prenom, mail, id, mdp, dateInscription);
	}

	/**
	 * Méthode permettant au Stagiaire de signer sa présence au Cours ayant pour
	 * Id coursId gâce à une clé de signature.
	 * 
	 * @param cleSignature
	 *            clé de signature à insérer pour valider la signature.
	 * @param coursId
	 *            numéro d'identifiant du Cours concerné.
	 * @return -2 si le Cours n'a pas lieu ce jour
	 * @return -1 si la clé de signature est incorrecte
	 * @return 1 si la signature est un succès.
	 */
	public int signer(String cleSignature, int coursId) {
		Cours coursASigner = getTabCours().get(Integer.valueOf(coursId));
		Date maintenant = new Date(System.currentTimeMillis());
		Calendar aujourd = Calendar.getInstance();
		Calendar calCours = Calendar.getInstance();
		aujourd.setTime(maintenant);
		for (Calendrier jourDeCours : coursASigner.getCalendriers()) {
			calCours.setTime(jourDeCours.getDate());
			if (calCours.get(Calendar.YEAR) == aujourd.get(Calendar.YEAR)
					&& calCours.get(Calendar.DAY_OF_YEAR) == aujourd.get(Calendar.DAY_OF_YEAR)) {
				if (cleSignature == jourDeCours.getCleSignature()) {
					jourDeCours.setAbsent(Boolean.FALSE);
					jourDeCours.setDateSignature(maintenant);
					return 1;
				} else {
					return -1;
				}
			}
		}
		return -2;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(super.toString());
		builder.deleteCharAt(super.toString().length() - 1);
		builder.append(", Stagiaire]");
		return builder.toString();
	}

}
