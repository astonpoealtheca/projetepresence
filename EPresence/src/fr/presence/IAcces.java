package fr.presence;

/**
 * Interface avec les informations pour la base de donn�es.
 * @author Hatim ZINOUNE
 *
 */

public interface IAcces {

	public static final String DB_DRIVER = "com.mysql.jdbc.Driver";
	public static final String DB_URL = "jdbc:mysql://10.3.110.60/Epresencetest?useSSL=false"; // 10.3.110.60 adresse IP du PC de amina
	public static final String DB_LOGIN = "root";
	public static final String DB_PWD = "root";

}
