package fr.presence;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Class représentant un cours
 * 
 * @author David Guery
 *
 */
public class Cours {
	/**
	 * Intitule du cours
	 */
	private String intitule;
	/**
	 * identifiant unique de chaque cours générer par la base de données
	 */
	private int id;
	/**
	 * date de début du cours
	 */
	private Date dateDebut;
	/**
	 * date de fin du cours
	 */
	private Date dateFin;
	/**
	 * liste des jours où le cours à lieu pour chaque stagiaire
	 */
	private List<Calendrier> calendriers = new ArrayList<>();

	/**
	 * Contsructeur par défault de la class
	 */
	public Cours() {
		this(null, -1, null, null);
	}

	/**
	 * Constructeur de la class avec tous les paramètres
	 * 
	 * @param intitule
	 * @param id
	 * @param dateDebut
	 * @param dateFin
	 */
	public Cours(String intitule, int id, Date dateDebut, Date dateFin) {
		super();
		setIntitule(intitule);
		setId(id);
		setDateDebut(dateDebut);
		setDateFin(dateFin);
	}

	public String getIntitule() {
		return intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	public Date getDateFin() {
		return dateFin;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}

	public List<Calendrier> getCalendriers() {
		return calendriers;
	}

	/**
	 * méthode qui génère des calendrier pour le Cours entre la date de début et
	 * la date de fin
	 */
	public void genererCalendrier() {
		GregorianCalendar gcal = new GregorianCalendar();
		gcal.setTime(getDateDebut());
		while (!gcal.getTime().after(getDateFin())) {
			Calendrier cal = new Calendrier(gcal.getTime(), null, null, null, getId());
			getCalendriers().add(cal);
			gcal.add(Calendar.DAY_OF_MONTH, 1);
		}
	}

	/**
	 * méthode permettant d'ajouter un calendrier à la liste de calendrier du
	 * cours.
	 * 
	 * @param cal
	 *            un Calendrier à ajouter
	 */
	public void ajouterCalendrier(Calendrier cal) {
		getCalendriers().add(cal);
	}

	/**
	 * méthode permetttant de retirer un Calendrier de la liste de calendrier du
	 * cours.
	 * 
	 * @param cal
	 *            un Calendrier à retirer
	 */
	public void retirerCalendrier(Calendrier cal) {
		getCalendriers().remove(cal);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Cours [intitule=");
		builder.append(intitule);
		builder.append(", id=");
		builder.append(id);
		builder.append(", dateDebut=");
		builder.append(dateDebut);
		builder.append(", dateFin=");
		builder.append(dateFin);
		builder.append("]");
		return builder.toString();
	}


}
