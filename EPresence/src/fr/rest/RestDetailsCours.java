package fr.rest;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import fr.bd.AccesBdCours;
import fr.bd.AccesBdUtilisateur;
import fr.presence.Cours;
import fr.presence.IAcces;
import fr.presence.Stagiaire;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Path("/detailsCours")
public class RestDetailsCours {

	/**
	 * Méthode de récupération des données utilisateur en utilisant le protocole
	 * HTTP GET.
	 * 
	 * @param userId
	 *            l'identifiant de l'utilisateur.
	 * @return userData un objet JSON { "id":"", "nom": "", "prenom": "", "mail": "",
	 *         "dateInscription": "dd MMMMM yyyy" } contenant les informations
	 *         relative à l'utilisateur connecté.
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response coursStagiaires(@QueryParam("stagiaireId") Integer stagiaireId) {
		Status status;
		JSONObject crsStagiaireData = new JSONObject();
		JSONArray stagiairesData = new JSONArray();
		JSONObject lesStagiaires = new JSONObject();
		AccesBdCours abdCrs = null;
		
		if(stagiaireId == null){
			status = Status.BAD_REQUEST;
			crsStagiaireData.element("Erreur", "stagiaireId null !");
			return Response.status(status).entity(crsStagiaireData.toString()).build();
		}
		
		try {
			abdCrs = new AccesBdCours(IAcces.DB_DRIVER);
			abdCrs.seConnecter(IAcces.DB_URL, IAcces.DB_LOGIN, IAcces.DB_PWD);
			List<Cours> cours = abdCrs.selectCours(stagiaireId);
			if (cours == null) {
				status = Status.BAD_REQUEST;
				crsStagiaireData.element("Erreur", "Liste vide !");
				return Response.status(status).entity(crsStagiaireData.toString()).build();
			} else {
				for (Cours crs : cours) {
					crsStagiaireData.element("intitule", crs.getIntitule());
					/*
					crsStagiaireData.element("nom", crs.getNom());
					crsStagiaireData.element("prenom", crs.getPrenom());
					crsStagiaireData.element("mail", crs.getMail());
					crsStagiaireData.element("nbrAbsences", abdCrs.calculerNbrAbsences(crs.getId()));
					SimpleDateFormat sdf = new SimpleDateFormat("dd MMMMM yyyy");
					crsStagiaireData.element("dateInscription", sdf.format(crs.getDateInscription()));
					stagiairesData.add(crsStagiaireData);
					
					lesStagiaires.element("stagiaires", stagiairesData);
					*/
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			status = Status.BAD_GATEWAY;
			crsStagiaireData.element("Erreur", "Connexion à la base de sonnées impossible ! + (" + e.getMessage() + ")");
			return Response.status(status).entity(crsStagiaireData.toString()).build();
		} finally {
			if (abdCrs != null) {
				abdCrs.seDeconnecter();
			}
		}
		
		status = Status.OK;
		return Response.status(status).entity(crsStagiaireData.toString()).build();
	}
	
}
