package fr.rest;

import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import fr.bd.AccesBdUtilisateur;
import fr.presence.IAcces;
import net.sf.json.JSONObject;

/**
 * Rest de génération de la clé de signature en PUT.
 * 
 * @author David Guery
 *
 */
@Path("/genererCleSignature")
public class RestGenererCleSignature {
	/**
	 * Méthode qui génére une clé aléatoire et la pousse dans la base pour tous
	 * les calendrier du cours à la date d'aujourd'hui
	 * 
	 * @param entry
	 *            un objet JSON de la forme : {"userId":"","coursId":""}
	 * @return un objet JSON contenant la clé générée :
	 *         {"cleSignature":"GrMVxaqk"}
	 */
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response genererCleSignature(String entry) {
		Status status;
		JSONObject keyData = new JSONObject();
		JSONObject key = JSONObject.fromObject(entry);
		Integer userId = Integer.valueOf(key.getInt("userId"));
		Integer coursId = Integer.valueOf(key.getInt("coursId"));
		if (userId == null || coursId == null) {
			status = Status.BAD_REQUEST;
			keyData.element("Erreur", "Identifiant de cours ou d'utilisateur incorrect");
			return Response.status(status).entity(keyData.toString()).build();
		}
		String resultat = null;
		AccesBdUtilisateur abdUtil = null;
		try {
			abdUtil = new AccesBdUtilisateur(IAcces.DB_DRIVER);
			abdUtil.seConnecter(IAcces.DB_URL, IAcces.DB_LOGIN, IAcces.DB_PWD);
			resultat = abdUtil.genererCode(userId.intValue(), coursId.intValue());
		} catch (SQLException e) {
			e.printStackTrace();
			status = Status.BAD_GATEWAY;
			keyData.element("Erreur", "Connexion à la base de données impossible ! (" + e.getMessage() + ")");
			return Response.status(status).entity(keyData.toString()).build();
		} finally {
			if (abdUtil != null) {
				abdUtil.seDeconnecter();
			}
		}
		if (resultat == null) {
			status = Status.NOT_FOUND;
			keyData.element("Erreur", "Echec de la génération de clé");
			return Response.status(status).entity(keyData.toString()).build();
		}
		if (resultat.equals("-1")) {
			status = Status.BAD_REQUEST;
			keyData.element("Erreur", "Cours invalide");
			return Response.status(status).entity(keyData.toString()).build();
		} else if (resultat.equals("-2")) {
			status = Status.BAD_REQUEST;
			keyData.element("Erreur", "Ce cours n'a pas lieu aujourd'hui");
			return Response.status(status).entity(keyData.toString()).build();
		} else {
			status = Status.OK;
			keyData.element("cleSignature", resultat);
			return Response.status(status).entity(keyData.toString()).build();
		}
	}

}
