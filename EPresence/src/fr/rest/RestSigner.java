package fr.rest;

import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import fr.bd.AccesBdAdresseIp;
import fr.bd.AccesBdUtilisateur;
import fr.presence.EpresenceException;
import fr.presence.IAcces;
import net.sf.json.JSONObject;

@Path("/signer")
public class RestSigner {
	/**
	 * Méthode qui enregistre la date de signature, vérifie et enregistre l'IP,
	 * et passe le statut absent à false.
	 * 
	 * @param entry
	 *            un JSON de la forme :
	 *            {"userId":"","coursId":"","cleSignature":""}
	 * @param request
	 *            la requete HTTP.
	 * @return un JSON contenant la confirmation ou l'erreur en fonction de la
	 *         réussite ou de l'échec de la signature.
	 */

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response signer(String entry, @Context HttpServletRequest request) {
		Status status;
		JSONObject signData = new JSONObject();
		JSONObject sign = JSONObject.fromObject(entry);
		Integer coursId = Integer.valueOf(sign.getInt("coursId"));
		Integer userId = Integer.valueOf(sign.getInt("userId"));
		Date dateJour = new Date(System.currentTimeMillis());
		String cleSignature = sign.getString("cleSignature");
		String adresseIp = request.getRemoteAddr();

		if (coursId == null || userId == null) {
			status = Status.EXPECTATION_FAILED;
			signData.element("Erreur", "Identifiant non renseigné");
			return Response.status(status).entity(signData.toString()).build();
		}
		if (cleSignature == null || cleSignature.trim().isEmpty()) {
			status = Status.EXPECTATION_FAILED;
			signData.element("Erreur", "Veuillez entrer une clé de signature.");
			return Response.status(status).entity(signData.toString()).build();
		}
		AccesBdAdresseIp abdIp = null;
		List<String> listIp = null;
		try {
			abdIp = new AccesBdAdresseIp(IAcces.DB_DRIVER);
			abdIp.seConnecter(IAcces.DB_URL, IAcces.DB_LOGIN, IAcces.DB_PWD);
			listIp = abdIp.selectAdressIp();
		} catch (SQLException e1) {
			e1.printStackTrace();
			status = Status.BAD_GATEWAY;
			signData.element("Erreur", "Connexion à la base de donnée impossible ! (" + e1.getMessage() + ")");
			return Response.status(status).entity(signData.toString()).build();
		} finally {
			if (abdIp != null) {
				abdIp.seDeconnecter();
			}
		}

		if (listIp != null && !listIp.contains(adresseIp)) {
			status = Status.BAD_GATEWAY;
			signData.element("Erreur", "Veuillez signer votre présence depuis un poste de l'établissement.");
			return Response.status(status).entity(signData.toString()).build();
		}

		AccesBdUtilisateur abdUtil = null;
		Integer res = null;
		try {
			abdUtil = new AccesBdUtilisateur(IAcces.DB_DRIVER);
			abdUtil.seConnecter(IAcces.DB_URL, IAcces.DB_LOGIN, IAcces.DB_PWD);
			res = abdUtil.signer(coursId, userId, dateJour, cleSignature, adresseIp);
		} catch (SQLException e) {
			e.printStackTrace();
			status = Status.BAD_GATEWAY;
			signData.element("Erreur", "Connexion à la base de donnée impossible ! (" + e.getMessage() + ")");
			return Response.status(status).entity(signData.toString()).build();
		} catch (EpresenceException e) {
			e.printStackTrace();
			status = Status.EXPECTATION_FAILED;
			signData.element("Erreur", e.getMessage());
			return Response.status(status).entity(signData.toString()).build();
		} finally {
			if (abdUtil != null) {
				abdUtil.seDeconnecter();
			}
		}
		if (res == null) {
			status = Status.NOT_FOUND;
			signData.element("Erreur", "Echec de connexion");
			return Response.status(status).entity(signData.toString()).build();
		}
		if (res.intValue() == 0) {
			status = Status.BAD_REQUEST;
			signData.element("Erreur", "Echec de la signature");
			return Response.status(status).entity(signData.toString()).build();
		}
		status = Status.OK;
		signData.element("Ok", "Vous êtes authentifier");
		return Response.status(status).entity(signData.toString()).build();
	}
}
