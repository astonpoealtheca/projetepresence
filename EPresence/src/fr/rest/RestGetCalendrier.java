package fr.rest;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import fr.bd.AccesBdCalendrier;
import fr.bd.CalendrierLecture;
import fr.presence.IAcces;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * Récupération des calendrier pour un utilisateur en GET.
 * 
 * @author David Guery
 *
 */
@Path("/getCalendrier")
public class RestGetCalendrier {
	/**
	 * Méthode qui récupère les calendriers d'un utilisateur et retourne un JSON
	 * au format fullcalendar.
	 * 
	 * @param userId
	 *            l'identifiant de l'utilisateur.
	 * @return {"events":[{"title":"","start":"","color":} ,{"title":""
	 *         ,"start":"","color":},{"title":"" ,"start":"","color":}]}
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getInfoUtilisateur(@QueryParam("userId") Integer userId) {
		Status status;
		JSONObject calData = new JSONObject();
		JSONArray calJson = new JSONArray();
		if (userId == null) {
			status = Status.BAD_REQUEST;
			calData.element("Erreur", "Identifiant null");
			return Response.status(status).entity(calData.toString()).build();
		} else {
			List<CalendrierLecture> listCal;
			AccesBdCalendrier abdCal = null;
			try {
				abdCal = new AccesBdCalendrier(IAcces.DB_DRIVER);
				abdCal.seConnecter(IAcces.DB_URL, IAcces.DB_LOGIN, IAcces.DB_PWD);
				listCal = abdCal.selectCalendrier(null, userId, null);
			} catch (SQLException e) {
				status = Status.BAD_GATEWAY;
				calData.element("Erreur", "Connexion à la base de données impossible! (" + e.getMessage() + ")");
				return Response.status(status).entity(calData.toString()).build();
			} finally {
				if (abdCal != null) {
					abdCal.seDeconnecter();
				}
			}
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			for (CalendrierLecture calendrier : listCal) {
				JSONObject jour = new JSONObject();
				System.out.println(calendrier.getIntituleCours());
				jour.element("title", calendrier.getIntituleCours());
				jour.element("start", sdf.format(calendrier.getDate()));
				jour.element("color", calendrier.getCoursId());
				calJson.add(jour);
			}
			calData.element("events", calJson);

		}
		status = Status.OK;
		System.out.println(calData.toString());
		return Response.status(status).entity(calData.toString()).build();
	}
}
