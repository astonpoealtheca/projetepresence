package fr.rest;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import fr.bd.AccesBdUtilisateur;
import fr.presence.IAcces;
import fr.presence.Stagiaire;
import fr.presence.Utilisateur;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Path("/details")
public class RestDetails {
	
	/**
	 * Méthode de récupération des données utilisateur en utilisant le protocole
	 * HTTP GET.
	 * 
	 * @param userId
	 *            l'identifiant de l'utilisateur.
	 * @return userData un objet JSON { "id":"", "nom": "", "prenom": "", "mail": "",
	 *         "dateInscription": "dd MMMMM yyyy" } contenant les informations
	 *         relative à l'utilisateur connecté.
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getInfoStagiaire() {
		Status status;
		JSONObject stagiaireData = new JSONObject();
		JSONArray stagiairesData = new JSONArray();
		JSONObject lesStagiaires = new JSONObject();
		AccesBdUtilisateur abdUtil = null;
		
		try {
			abdUtil = new AccesBdUtilisateur(IAcces.DB_DRIVER);
			abdUtil.seConnecter(IAcces.DB_URL, IAcces.DB_LOGIN, IAcces.DB_PWD);
			List<Stagiaire> stagiaires = abdUtil.selectAllStagiaire();
			if (stagiaires == null) {
				status = Status.BAD_REQUEST;
				stagiaireData.element("Erreur", "Liste vide !");
				return Response.status(status).entity(stagiaireData.toString()).build();
			} else {
				for (Stagiaire stagiaire : stagiaires) {
					
					stagiaireData.element("id", stagiaire.getId());
					stagiaireData.element("nom", stagiaire.getNom());
					stagiaireData.element("prenom", stagiaire.getPrenom());
					stagiaireData.element("mail", stagiaire.getMail());
					stagiaireData.element("nbrAbsences", abdUtil.calculerNbrAbsences(stagiaire.getId()));
					SimpleDateFormat sdf = new SimpleDateFormat("dd MMMMM yyyy");
					stagiaireData.element("dateInscription", sdf.format(stagiaire.getDateInscription()));
					stagiairesData.add(stagiaireData);
					
					lesStagiaires.element("stagiaires", stagiairesData);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			status = Status.BAD_GATEWAY;
			stagiaireData.element("Erreur", "Connexion à la base de sonnées impossible ! + (" + e.getMessage() + ")");
			return Response.status(status).entity(stagiaireData.toString()).build();
		} finally {
			if (abdUtil != null) {
				abdUtil.seDeconnecter();
			}
		}
		
		status = Status.OK;
		return Response.status(status).entity(lesStagiaires.toString()).build();
	}
	
}
