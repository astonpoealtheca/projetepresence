package fr.rest;

import java.sql.SQLException;

import javax.mail.MessagingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import fr.bd.AccesBdUtilisateur;
import fr.presence.Formateur;
import fr.presence.IAcces;
import net.sf.json.JSONObject;

@Path("/pwdOublie")
public class pwdOublie {

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response authentification(String entry) {
		Status status;
		JSONObject userData = new JSONObject();
		JSONObject user = JSONObject.fromObject(entry);
		String mail = user.getString("mail");
		if (mail == null || mail.trim().isEmpty()) {
			status = Status.BAD_REQUEST;
			userData.element("Erreur", "Veuillez entrer une adresse e-mail");
			return Response.status(status).entity(userData.toString()).build();
		} else {
			AccesBdUtilisateur abdUtil = null;
			try {
				abdUtil = new AccesBdUtilisateur(IAcces.DB_DRIVER);
				abdUtil.seConnecter(IAcces.DB_URL, IAcces.DB_LOGIN, IAcces.DB_PWD);
				Formateur form = new Formateur();
				String nvMdp = form.randomString(10);
				Integer userId = abdUtil.selectUtilisateurId(mail);
				int res = abdUtil.modifierMdp(userId.intValue(), nvMdp, nvMdp);
				if (res == -3) {
					status = Status.BAD_REQUEST;
					userData.element("Erreur", "Utilisateur inconnu.");
					return Response.status(status).entity(userData.toString()).build();
				} else if (res == -2) {
					status = Status.BAD_REQUEST;
					userData.element("Erreur", "Le mot de passe actuel est erroné.");
					return Response.status(status).entity(userData.toString()).build();
				} else if (res == -1) {
					status = Status.BAD_REQUEST;
					userData.element("Erreur", "La confirmation et le nouveau mot de passe sont différent.");
					return Response.status(status).entity(userData.toString()).build();
				} else {
					SendEMail mailSender = new SendEMail();
					mailSender.send(mail, nvMdp);
					status = Status.OK;
					userData.element("Ok", "Un e-mail vient de vous être envoyé avec votre nouveau mot de passe");
				}
			} catch (SQLException e) {
				status = Status.BAD_GATEWAY;
				userData.element("Erreur", "Connexion à la base de donnée impossible! (" + e.getMessage() + ")");
				e.printStackTrace();
				return Response.status(status).entity(userData.toString()).build();
			} catch (MessagingException e) {
				status = Status.NOT_FOUND;
				userData.element("Erreur", "Envoi du message impossible! (" + e.getMessage() + ")");
				return Response.status(status).entity(userData.toString()).build();
			} finally {
				if (abdUtil != null) {
					abdUtil.seDeconnecter();
				}
			}
		}
		System.out.println(userData.toString());
		return Response.status(status).entity(userData.toString()).build();
	}
}
