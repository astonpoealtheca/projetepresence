package fr.rest;

import java.sql.SQLException;

import javax.mail.MessagingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import fr.bd.AccesBdUtilisateur;
import fr.presence.IAcces;
import fr.presence.Utilisateur;
import net.sf.json.JSONObject;

@Path("/modifierPass")
public class RestModifierPass {
	/**
	 * Méthode pour modifier le mot de passe d'un utilisateur.
	 * 
	 * @param entry
	 *            un objet JSON de la forme
	 *            {"userId":"","exMdp":"","nvMdp":"","nvMdpConf":""}. --userId :
	 *            identifiant de l'utilisateur. --exMdp : mot de passe actuel.
	 *            --nvMdp : nouveau mot de passe. --nvMdpCong : confirmation du
	 *            nouveau mot de passe
	 * @return un JSON avec un message de succès ({"Ok": "message"}) ou d'erreur
	 *         ({"Erreur":"message"}). Envoie un mail automatiquement à
	 *         l'utilisateur avec le nouveau mot de passe.
	 */
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response modifierMotDePasse(String entry) {
		Status status;
		JSONObject userData = new JSONObject();
		JSONObject user = JSONObject.fromObject(entry);
		System.out.println("recupératin du JSON");
		Integer id = Integer.valueOf(user.getInt("userId"));
		String exMdp = user.getString("exMdp");
		String nvMdp = user.getString("nvMdp");
		String nvMdpConf = user.getString("nvMdpConf");
		if (id == null) {
			status = Status.EXPECTATION_FAILED;
			userData.element("Erreur", "Vous avez été déconnecté");
			userData.element("url", "/login.html");
			return Response.status(status).entity(userData.toString()).build();
		} else if (exMdp == null || exMdp.trim().isEmpty()) {
			status = Status.BAD_REQUEST;
			userData.element("Erreur", "Veuillez entrer votre mot de passe");
			return Response.status(status).entity(userData.toString()).build();
		} else if (nvMdp == null || nvMdpConf == null || nvMdp.trim().isEmpty() || nvMdpConf.trim().isEmpty()) {
			status = Status.BAD_REQUEST;
			userData.element("Erreur", "Veuillez renseigner tous les champs");
			return Response.status(status).entity(userData.toString()).build();
		} else {
			AccesBdUtilisateur abdUtil = null;
			try {
				System.out.println("Initialisatoin de la Bd");
				abdUtil = new AccesBdUtilisateur(IAcces.DB_DRIVER);
				System.out.println("Connection à la base de données");
				abdUtil.seConnecter(IAcces.DB_URL, IAcces.DB_LOGIN, IAcces.DB_PWD);
				System.out.println("Recherche du mail");
				Utilisateur util = abdUtil.selectUtilisateur(id.intValue());
				int res = 0;
				String mail = null;
				if (util == null) {
					status = Status.NOT_FOUND;
					userData.element("Erreur", "Identifiant utilisateur incorrect !");
					return Response.status(status).entity(userData.toString()).build();
				} else {
					mail = util.getMail();
					System.out.println(mail);
					res = abdUtil.authentifier(mail, exMdp);
				}
				if (res == -2) {
					status = Status.EXPECTATION_FAILED;
					userData.element("Erreur", "Mot de passe incorrect");
					return Response.status(status).entity(userData.toString()).build();
				} else {
					int resu = abdUtil.modifierMdp(id.intValue(), nvMdp, nvMdpConf);
					if (resu == -1) {
						status = Status.BAD_REQUEST;
						userData.element("Erreur", "Le nouveau mot de passe et sa confirmation sont différent");
						return Response.status(status).entity(userData.toString()).build();
					} else if (resu == 1) {
						userData.element("Ok", "Votre mot de passe a été changé.");
						SendEMail emailService = new SendEMail();
						emailService.send(mail, nvMdp);
						status = Status.OK;
						return Response.status(status).entity(userData.toString()).build();
					} else {
						status = Status.BAD_REQUEST;
						userData.element("Erreur", "Erreur de procédure interne");
						return Response.status(status).entity(userData.toString()).build();
					}
				}
			} catch (SQLException e) {
				status = Status.BAD_GATEWAY;
				userData.element("Erreur", "Connexion à la base de données impossible! (" + e.getMessage() + ")");
				return Response.status(status).entity(userData.toString()).build();
			} catch (MessagingException e) {
				status = Status.NOT_FOUND;
				userData.element("Erreur", "Problème d'envoie de mail (" + e.getMessage() + ")");
				return Response.status(status).entity(userData.toString()).build();
			} finally {
				if (abdUtil != null) {
					abdUtil.seDeconnecter();
				}
			}
		}

	}
}

