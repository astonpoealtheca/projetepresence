package fr.rest;

import java.sql.SQLException;
import java.text.SimpleDateFormat;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import fr.bd.AccesBdUtilisateur;
import fr.presence.EpresenceException;
import fr.presence.IAcces;
import fr.presence.Utilisateur;
import net.sf.json.JSONObject;

@Path("/infoUtilisateur")
public class RestInfoUtilisateur {
	/**
	 * Méthode de récupération des données utilisateur en utilisant le protocole
	 * HTTP GET.
	 * 
	 * @param userId
	 *            l'identifiant de l'utilisateur.
	 * @return userData un objet JSON { "nom": "", "prenom": "", "mail": "",
	 *         "dateInscription": "dd MMMMM yyyy" } contenant les informations
	 *         relative à l'utilisateur connecté.
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getInfoUtilisateur(@QueryParam("userId") Integer userId) {
		Status status;
		JSONObject userData = new JSONObject();
		if (userId == null) {
			status = Status.BAD_REQUEST;
			userData.element("Erreur", "Identifiant null");
			return Response.status(status).entity(userData.toString()).build();
		} else {
			AccesBdUtilisateur abdUtil = null;
			try {
				abdUtil = new AccesBdUtilisateur(IAcces.DB_DRIVER);
				abdUtil.seConnecter(IAcces.DB_URL, IAcces.DB_LOGIN, IAcces.DB_PWD);
				Utilisateur utilisateur = abdUtil.selectUtilisateur(userId.intValue());
				if (utilisateur == null) {
					status = Status.BAD_REQUEST;
					userData.element("Erreur", "Identifiant inconnu");
					return Response.status(status).entity(userData.toString()).build();
				} else {
					userData.element("nom", utilisateur.getNom());
					userData.element("prenom", utilisateur.getPrenom());
					userData.element("mail", utilisateur.getMail());
					SimpleDateFormat sdf = new SimpleDateFormat("dd MMMMM yyyy");
					userData.element("dateInscription", sdf.format(utilisateur.getDateInscription()));
				}
			} catch (SQLException e) {
				e.printStackTrace();
				status = Status.BAD_GATEWAY;
				userData.element("Erreur", "Connexion à la base de sonnées impossible ! + (" + e.getMessage() + ")");
				return Response.status(status).entity(userData.toString()).build();
			} finally {
				if (abdUtil != null) {
					abdUtil.seDeconnecter();
				}
			}
		}
		status = Status.OK;
		System.out.println(userData.toString());
		return Response.status(status).entity(userData.toString()).build();
	}
}
