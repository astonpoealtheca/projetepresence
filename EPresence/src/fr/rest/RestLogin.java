package fr.rest;

import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import fr.bd.AccesBdUtilisateur;
import fr.bd.Administrateur;
import fr.presence.Formateur;
import fr.presence.IAcces;
import fr.presence.Stagiaire;
import fr.presence.Utilisateur;
import net.sf.json.JSONObject;

/**
 * Service REST implémenté en jersey pour l'authentification d'un utilisateur à
 * l'application
 * 
 * @author David Guery
 *
 */
@Path("/login")
public class RestLogin {

	/**
	 * Méthode d'authentification à l'application en utilisant le protocole HTTP
	 * POST.
	 * 
	 * @param entry
	 *            un objet sous format JSON
	 *            {"mail":"unMail","password":"unMotDePasse"} représentant les
	 *            données entré par l'utilisateur pour s'authentifier.
	 * @return userData un objet JSON
	 *         {"nom":"","prenom":"","mail":"","id":,"dateInscription":,"role":
	 *         "","url":""} contenant les informations relative à l'utilisateur
	 *         connecté.
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response authentification(String entry) {
		Status status;
		JSONObject userData = new JSONObject();
		JSONObject user = JSONObject.fromObject(entry);
		String mail = user.getString("mail");
		String pwd = user.getString("motdepasse");
		if (mail == null || pwd == null || mail.trim().isEmpty() || pwd.trim().isEmpty()) {
			status = Status.BAD_REQUEST;
			userData.element("Erreur", "Veuillez entrer un mot de passe et un login");
			return Response.status(status).entity(userData.toString()).build();
		} else {
			AccesBdUtilisateur abdUtil = null;
			try {
				abdUtil = new AccesBdUtilisateur(IAcces.DB_DRIVER);
				abdUtil.seConnecter(IAcces.DB_URL, IAcces.DB_LOGIN, IAcces.DB_PWD);
				int resultat = abdUtil.authentifier(mail, pwd);
				System.out.println(resultat);
				if (resultat > 0) {
					Utilisateur utilisateur = abdUtil.selectUtilisateur(resultat);
					if (utilisateur == null) {
						status = Status.BAD_REQUEST;
						userData.element("Erreur", "Identifiant inconnu");
						return Response.status(status).entity(userData.toString()).build();
					}
					System.out.println(utilisateur);
					if (utilisateur instanceof Stagiaire) {
						userData.element("id", utilisateur.getId());
						userData.element("url", "/stagiaire.html");
					} else if (utilisateur instanceof Formateur) {
						userData.element("id", utilisateur.getId());
						userData.element("url", "/formateur.html");
					} else if (utilisateur instanceof Administrateur) {
						userData.element("id", utilisateur.getId());
						userData.element("url", "/admin.html");
					}
				} else if (resultat == -1) {
					status = Status.BAD_REQUEST;
					userData.element("Erreur", "Ce mail n'est pas reconnu");
					return Response.status(status).entity(userData.toString()).build();
				} else {
					status = Status.BAD_REQUEST;
					userData.element("Erreur", "Mot de passe erroné");
					System.out.println(userData.toString());
					return Response.status(status).entity(userData.toString()).build();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				status = Status.BAD_GATEWAY;
				userData.element("Erreur", "Connexion à la base de sonnées impossible ! + (" + e.getMessage() + ")");
				return Response.status(status).entity(userData.toString()).build();
			} finally {
				if (abdUtil != null) {
					abdUtil.seDeconnecter();
				}
			}
		}
		status = Status.OK;
		System.out.println(userData.toString());
		return Response.status(status).entity(userData.toString()).build();
	}
}
