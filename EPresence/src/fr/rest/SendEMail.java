package fr.rest;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendEMail {

	public void send(String userMail, String password) throws MessagingException {
		Properties props = new Properties();
		props = System.getProperties();
		props.put("mail.smtp.port", "587");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");

		Session session = Session.getDefaultInstance(props, null);

		StringBuffer msgBody = new StringBuffer("Votre nouveau mot de passe d'accès à stone éducation est ");
		msgBody.append(password);
		msgBody.append("\n");
		msgBody.append("Pensez à le modifier.");


		Message msg = new MimeMessage(session);
		Transport transport = null;
		try {
			msg.addRecipient(Message.RecipientType.TO, new InternetAddress(userMail));
			msg.setSubject("Test d'envoie de message avec java");
			msg.setText(msgBody.toString());
			transport = session.getTransport("smtp");
			transport.connect("smtp.gmail.com", "stoneeduc@gmail.com", "2016aston");
			transport.sendMessage(msg, msg.getAllRecipients());
		} catch (AddressException e) {
			throw e;
		} catch (NoSuchProviderException e) {
			throw e;
		} catch (MessagingException e) {
			throw e;
		} finally {
			if (transport != null) {
				try {
					transport.close();
				} catch (MessagingException e) {
					throw e;
				}
			}

		}



	}

}
