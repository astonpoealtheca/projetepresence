package fr.bd;

import java.util.Date;

import fr.presence.Utilisateur;

/**
 * Class de lecture des utilisateur avec le role administrateur. Hérite de
 * Utilisateur.
 * 
 * @author David Guery
 *
 */
public class Administrateur extends Utilisateur {

	/**
	 * Constructeur par défaut hérité de Utilisateur.
	 */
	public Administrateur() {
		super();
	}

	/**
	 * Constructeur prenant en compte tous les attribu de class hérité de
	 * Utilisateur.
	 * 
	 * @param nom
	 * @param prenom
	 * @param mail
	 * @param id
	 * @param mdp
	 * @param dateInscription
	 */
	public Administrateur(String nom, String prenom, String mail, Integer id, String mdp, Date dateInscription) {
		super(nom, prenom, mail, id, mdp, dateInscription);
	}




}
