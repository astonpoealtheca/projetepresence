package fr.bd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Cette classe fait un acces a une base de donnees.
 * @author Hatim ZINOUNE
 *
 */

abstract class AccesBD {

	protected Connection cxt;

	/**
	 * Constructeur de la classe qui fournit une connection. <br/>
	 *
	 * C'est dans cette methode qu'est charge le driver. <br/>
	 *
	 * @param aDriverName
	 *            nom du Driver a charger
	 * @throws SQLException
	 *             si une erreur survient
	 */
	public AccesBD(String aDriverName) throws SQLException {
		try {
			Class.forName(aDriverName);
		} catch (Throwable cnf) {
			throw new SQLException("Impossible de charger le driver '" + aDriverName + "'", cnf);
		}
	}

	/**
	 * M�thode de connexion de la base. <br/>
	 *
	 * C'est dans cette methode que l'on recupere un objet Connection
	 *
	 * @param aUrl
	 *            un url
	 * @param aLogin
	 *            un login
	 * @param aPassword
	 *            un mot de passe
	 */
	public void seConnecter(String aUrl, String aLogin, String aPassword) throws SQLException {
		this.cxt = DriverManager.getConnection(aUrl, aLogin, aPassword);
	}

	/**
	 * M�thode de d�connexion de la base. <br/>
	 *
	 * C'est dans cette methode que l'on ferme l'objet Connection
	 */
	public void seDeconnecter() {
		try {
			if (this.cxt != null) {
				this.cxt.close();
			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
	}

}
