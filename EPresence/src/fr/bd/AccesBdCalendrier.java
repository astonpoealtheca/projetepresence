package fr.bd;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Class d'accès à la base de données centré sur la table Calendrier. Hérite de
 * AccesBD.
 * 
 * @author David Guery
 *
 */
public class AccesBdCalendrier extends AccesBD {

	/**
	 * Constructeur de la class hérité de AccesBD.
	 * 
	 * @param aDriverName
	 * @throws SQLException
	 */
	public AccesBdCalendrier(String aDriverName) throws SQLException {
		super(aDriverName);
	}

	/**
	 * Méthode qui récupère la liste des jours où un cours a lieu
	 * 
	 * @param coursId
	 *            identifiant du cours, peut être null
	 * @param utilisateurId
	 *            indentifiant de l'utilisateur inscrit au Cours, peut être null
	 * @param dateJour
	 *            date du jour du Calendrier, peut être null
	 * @return List<Calendrier> une liste des jour de calendrier en fonction des
	 *         paramètres entré : --si les trois paramètres sont null la liste
	 *         contient la totalité des entrées la table Calendrier de la BD.
	 *         -si seul coursId est défini, la liste contient tous les jour où
	 *         ce cours à lieu. --si seul utilisateurId est défini, la liste
	 *         contient tous les calendriers de tous les cours auxquels est
	 *         inscrit l'utilisateur --si seul la date est défini, la liste
	 *         contient tous les calendrier du jours (combinaison coursId,
	 *         utilisateurId). --si coursId et utilisateurId sont renseigné
	 *         alors la liste contient tous les jours du cours auquel
	 *         l'utilisateur est inscrit -- si tout est renseigné la liste
	 *         contient un seul Calendrier pour ce cours, cet utilisateur, à
	 *         cette date.
	 * @throws SQLException
	 */
	public List<CalendrierLecture> selectCalendrier(Integer coursId, Integer utilisateurId, Date dateJour)
			throws SQLException {
		List<CalendrierLecture> listCalendrier = new ArrayList<>();
		if ((this.cxt != null) && !this.cxt.isClosed()) {
			PreparedStatement request = null;
			ResultSet resultat = null;
			try {
				StringBuilder requete = new StringBuilder(
						"select calendrier.*, cours.intitule from calendrier,cours where Cours_id=cours.id");
				if (coursId != null) {
					requete.append(" and Cours_id=? ");
				}
				if (utilisateurId != null) {
					requete.append(" and Utilisateur_id=?");
				}
				if (dateJour != null) {
					requete.append(" and `date`=?");
				}
				requete.append(";");
				request = cxt.prepareStatement(requete.toString());
				int intParam = 1;
				if (coursId != null) {
					request.setInt(intParam, coursId.intValue());
					intParam++;
				}
				if (utilisateurId != null) {
					request.setInt(intParam, utilisateurId.intValue());
					intParam++;
				}
				if (dateJour != null) {
					request.setDate(intParam, dateJour);
				}
				resultat = request.executeQuery();
				while (resultat.next()) {
					Date date = resultat.getDate("date");
					String cleSignature = resultat.getString("cleSignature");
					Boolean absent = Boolean.valueOf(resultat.getBoolean("absent"));
					Timestamp dateSignature = resultat.getTimestamp("dateSignature");
					Integer idCours = Integer.valueOf(resultat.getInt("Cours_id"));
					Integer idUtilisateur = Integer.valueOf(resultat.getInt("Utilisateur_id"));
					String intituleCours = resultat.getString("intitule");
					listCalendrier.add(new CalendrierLecture(date, cleSignature, absent, dateSignature,
							idCours.intValue(), idUtilisateur.intValue(), intituleCours));
				}
			} catch (SQLException e) {
				throw e;
			} finally {
				if (resultat != null) {
					try {
						resultat.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				if (request != null) {
					try {
						request.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
			return listCalendrier;
		} else {
			throw new SQLException("Connexion invalide!");
		}
	}
}
