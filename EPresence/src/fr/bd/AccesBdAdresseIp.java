package fr.bd;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class d'accès à la base de données centré sur la table AdresseIp. Hérite de
 * AccesBD.
 * 
 * @author David Guery
 *
 */
public class AccesBdAdresseIp extends AccesBD {

	/**
	 * Constructuer de la class hérité de AccesBD.
	 * 
	 * @param aDriverName
	 * @throws SQLException
	 */
	public AccesBdAdresseIp(String aDriverName) throws SQLException {
		super(aDriverName);
	}

	/**
	 * Méthode qui retourne la liste des adresse IP contenu dans la base de
	 * données.
	 * 
	 * @return List<String> la liste des adresse IP sous forme de chaine de
	 *         caractères.
	 * @throws SQLException
	 */
	public List<String> selectAdressIp() throws SQLException {
		List<String> listIp = new ArrayList<>();
		if ((this.cxt != null) && !this.cxt.isClosed()) {
			PreparedStatement request = null;
			ResultSet resultat = null;
			try {
				request = this.cxt.prepareStatement("SELECT numero FROM epresencetest.adresseip;");
				resultat = request.executeQuery();

				while (resultat.next()) {
					listIp.add(resultat.getString("numero"));
				}
			} catch (SQLException e) {
				throw e;
			} finally {
				if (resultat != null) {
					try {
						resultat.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				if (request != null) {
					try {
						request.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
			return listIp;
		}else

		{
			throw new SQLException("Connexion invalide!");
		}
	}

}
