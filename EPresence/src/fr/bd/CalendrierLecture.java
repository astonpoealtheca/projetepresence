package fr.bd;

import java.util.Date;

import fr.presence.Calendrier;

public class CalendrierLecture extends Calendrier {
	private String intituleCours;

	public CalendrierLecture(Date date, String cleSignature, Boolean absent, Date dateSignature, int coursId,
			String intituleCours) {
		super(date, cleSignature, absent, dateSignature, coursId);
		setIntituleCours(intituleCours);
	}

	public CalendrierLecture(Date date, String cleSignature, Boolean absent, Date dateSignature, int coursId,
			int utilisateurId, String intituleCours) {
		super(date, cleSignature, absent, dateSignature, coursId, utilisateurId);
		setIntituleCours(intituleCours);
	}

	public String getIntituleCours() {
		return intituleCours;
	}

	public void setIntituleCours(String intituleCours) {
		this.intituleCours = intituleCours;
	}


}
