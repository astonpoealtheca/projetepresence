package fr.bd;

import java.sql.Date;
import java.sql.Timestamp;

import fr.presence.Stagiaire;

/**
 * Class utilisé lors de la lecture de la base de données pour stocké la liste
 * des stagiaires absent ou présent. Hérite de stagiaire.
 * 
 * @author David Guery
 *
 */
public class StagiaireAbs extends Stagiaire {
	/**
	 * date et heure de signature du stagiaire si il a signé.
	 */
	private Timestamp dateSignature;
	/**
	 * date à laquelle à lieu le cours.
	 */
	private Date dateCour;

	/**
	 * Constructeur de la class permettant d'initialiser tous les attributs.
	 * 
	 * @param nom
	 *            Nom du stagiaire
	 * @param prenom
	 *            Prenom du stagiaire
	 * @param dateSignature
	 *            date et heure de signature
	 * @param dateCour
	 *            date à laquelle à lieu le cours.
	 */
	public StagiaireAbs(String nom, String prenom, Timestamp dateSignature, Date dateCour) {
		super(nom, prenom, null, null, null, null);
		this.dateSignature = dateSignature;
		this.dateCour = dateCour;
	}

	public Timestamp getDateSignature() {
		return dateSignature;
	}

	public void setDateSignature(Timestamp dateSignature) {
		this.dateSignature = dateSignature;
	}

	public Date getDateCour() {
		return dateCour;
	}

	public void setDateCour(Date dateCour) {
		this.dateCour = dateCour;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(super.toString());
		builder.deleteCharAt(super.toString().length() - 1);
		builder.append(", dateSignature=");
		builder.append(dateSignature);
		builder.append(", dateCours=");
		builder.append(dateCour);
		builder.append("]");
		return builder.toString();
	}

}
