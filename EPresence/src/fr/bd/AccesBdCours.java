package fr.bd;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import fr.presence.Calendrier;
import fr.presence.Cours;
import fr.presence.EpresenceException;

/**
 * Class d'accès à la base de donnée centré sur la table Cours de la base. Hérite de AccesBD.
 * 
 * @author David Guery
 *
 */
public class AccesBdCours extends AccesBD {

	/**
	 * Constructeur de la class hérité de AccesBd.
	 * 
	 * @param aDriverName
	 *            nom du driver de la base de données.
	 * @throws SQLException
	 */
	public AccesBdCours(String aDriverName) throws SQLException {
		super(aDriverName);
	}

	/**
	 * Méthode qui récupère la liste de tous les cours enregistrer dans l'ecole,
	 * à utiliser dans le cas d'un administrateur
	 * 
	 * @return List<Cours> la liste des cours
	 * @throws SQLException
	 */
	public List<Cours> selectAllCours() throws SQLException {
		return this.selectCours(null);
	}

	/**
	 * Méthode qui récupère la liste des Cours d'un utilisateur, à utiliser dans
	 * le cas d'un stagiaire ou d'un formateur
	 * 
	 * @param utilisateurId
	 *            identifiant de l'utilisateur
	 * @return List<Cours> la liste des cours de l'utilisateur
	 * @throws SQLException
	 */
	public List<Cours> selectCours(Integer utilisateurId) throws SQLException {
		List<Cours> listCours = new ArrayList<>();
		if ((this.cxt != null) && !this.cxt.isClosed()) {
			PreparedStatement request = null;
			ResultSet resultat = null;
			try {
				StringBuilder requete = new StringBuilder("select cours.* from cours");
				if (utilisateurId != null) {
					requete.append(",calendrier where cours.id=Cours_id and Utilisateur_id=?");
				}
				requete.append(";");
				request = cxt.prepareStatement(requete.toString());
				if (utilisateurId != null) {
					request.setInt(1, utilisateurId.intValue());
				}
				resultat = request.executeQuery();
				while (resultat.next()) {
					// Récupération des attributs des Cours
					int id = resultat.getInt("id");
					String intitule = resultat.getString("intitule");
					Date dateDebut = resultat.getDate("dateDebut");
					Date dateFin = resultat.getDate("dateFin");
					Cours unCours = new Cours(intitule, id, dateDebut, dateFin);

					// ajout des Cours dans la liste de Cours
					listCours.add(unCours);
				}
			} catch (SQLException e) {
				throw e;
			} finally {
				if (resultat != null) {
					try {
						resultat.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				if (request != null) {
					try {
						request.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
			return listCours;
		} else {
			throw new SQLException("Connexion invalide!");
		}
	}



	/**
	 * Méthode qui insert un nouveau Cours dans la base de données
	 * 
	 * @param intitule
	 *            une String contenant l'intitule du Cours
	 * @param dateDebut
	 *            une java.sql.Date contenant la date de début du Cours
	 * @param dateFin
	 *            une java.sql.Date contenant la date de fin du Cours
	 * @throws SQLException
	 */
	public Integer insertCours(String intitule, Date dateDebut, Date dateFin) throws SQLException {
		Cours unCours = new Cours(intitule, 0, dateDebut, dateFin);
		return this.insertCours(unCours);
	}

	/**
	 * Méthode qui insert un nouveau Cours dans la base de données
	 * 
	 * @param unCours
	 *            une instance de la class Cours
	 * @throws SQLException
	 */
	public Integer insertCours(Cours unCours) throws SQLException {
		if ((this.cxt != null) && !this.cxt.isClosed()) {
			PreparedStatement request = null;
			Integer resultat = null;
			try {
				String requete = "insert into cours set intitule=?, dateDebut=?, dateFin=?;";
				request = cxt.prepareStatement(requete);
				request.setString(1, unCours.getIntitule());
				request.setDate(2, new Date(unCours.getDateDebut().getTime()));
				request.setDate(3, new Date(unCours.getDateDebut().getTime()));
				resultat = Integer.valueOf(request.executeUpdate());

			} catch (SQLException e) {
				throw e;
			} finally {
				if (request != null) {
					try {
						request.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
			return resultat;
		} else {
			throw new SQLException("Connexion invalide!");

		}
	}

	/**
	 * Méthode permettant de mettre à jour un cours existant dans la base de
	 * données.
	 * 
	 * @param unCours
	 *            Cours à mettre à jour.
	 * @return un entier en fonction de la réussite ou non de la mise à jour.
	 * @throws SQLException
	 */
	public Integer updateCours(Cours unCours) throws SQLException {
		if ((this.cxt != null) && !this.cxt.isClosed()) {
			PreparedStatement request = null;
			Integer resultat = null;
			try {
				// Construction de la requête d'update en fonction des paramètre
				StringBuilder requete = new StringBuilder("update cours set ");
				if (unCours.getIntitule() != null) {
					requete.append("intitule=?");
				}
				if (unCours.getDateDebut() != null) {
					if (unCours.getIntitule() != null)
						requete.append(",");
					requete.append("dateDebut=?");
				}
				if (unCours.getDateFin() != null) {
					if (unCours.getIntitule() != null || unCours.getDateDebut() != null)
						requete.append(",");
					requete.append("dateFin=?");
				}
				requete.append(" where id=?");
				request = cxt.prepareStatement(requete.toString());
				// Inclusion des paramètres dans la requete
				int intParam = 1;
				if (unCours.getIntitule() != null) {
					request.setString(intParam, unCours.getIntitule());
					intParam++;
				}
				if (unCours.getDateDebut() != null) {
					request.setDate(intParam, new Date(unCours.getDateDebut().getTime()));
					intParam++;
				}
				if (unCours.getDateFin() != null) {
					request.setDate(intParam, new Date(unCours.getDateFin().getTime()));
					intParam++;
				}
				request.setInt(intParam, unCours.getId());
				// Execution de l'update
				resultat = Integer.valueOf(request.executeUpdate());
			} catch (SQLException e) {
				throw e;
			} finally {
				if (request != null) {
					try {
						request.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
			return resultat;
		} else {
			throw new SQLException("Connexion invalide!");

		}
	}

	/**
	 * Methode permettant de calculer le nombre d'absent ou de présent pour un
	 * cours et/ou une date, ou total
	 * 
	 * @param coursId
	 *            l'identifiant du cours. Peut être pour prendre en compte la
	 *            totalité des cours.
	 * @param absPres
	 *            un boolean, si True recherche des absent, si False recherche
	 *            des présent.
	 * @param dateDebut
	 *            la date de début. Peut être null pour prendre en compte toutes
	 *            les dates jusqu'à la date de fin.
	 * @param dateFin
	 *            la date de fin. Peut être null pour prendr en compte toutes
	 *            les dates depuis la date de début. Si les deux dates sont null
	 *            alors par défault toutes les dates sont prisent en compte. Si
	 *            les deux dates sont identiques, seul ce jour est pris en
	 *            compte.
	 * @return un entier, le nombre de présent ou d'absent.
	 * @throws SQLException
	 */
	public Integer nbPresentAbsent(Integer coursId, Boolean absPres, Date dateDebut, Date dateFin) throws SQLException {
		Integer result = null;
		if ((this.cxt != null) && !this.cxt.isClosed()) {
			PreparedStatement request = null;
			ResultSet resultat = null;
			try {
				StringBuilder requete = new StringBuilder(
						"select count(Utilisateur_id) from calendrier where absent=?");
				if (coursId != null) {
					requete.append(" and Cours_id=?");
				}
				if (dateDebut != null) {
					requete.append(" and `date`>=?");
				}
				if (dateFin != null) {
					requete.append(" and `date`<=?");
				}
				requete.append(";");
				request = this.cxt.prepareStatement(requete.toString());
				int intParam = 1;
				request.setBoolean(intParam, absPres.booleanValue());
				intParam++;
				if (coursId != null) {
					request.setInt(intParam, coursId.intValue());
					intParam++;
				}
				if (dateDebut != null) {
					request.setDate(intParam, dateDebut);
					intParam++;
				}
				if (dateFin != null) {
					request.setDate(intParam, dateFin);
				}

				resultat = request.executeQuery();
				while (resultat.next()) {
					result = Integer.valueOf(resultat.getInt(1));
				}

			} catch (SQLException e) {
				throw e;
			} finally {
				if (resultat != null) {
					try {
						resultat.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				if (request != null) {
					try {
						request.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
			return result;
		} else {
			throw new SQLException("Connexion invalide!");
		}

	}

	/**
	 * Methode permettant d'obtenir la liste d'absent ou de présent pour un
	 * cours et/ou une date, ou total.
	 * 
	 * @param coursId
	 *            l'identifiant du cours. Peut être pour prendre en compte la
	 *            totalité des cours.
	 * @param absPres
	 *            un boolean, si True recherche des absent, si False recherche
	 *            des présent.
	 * @param dateDebut
	 *            la date de début. Peut être null pour prendre en compte toutes
	 *            les dates jusqu'à la date de fin.
	 * @param dateFin
	 *            la date de fin. Peut être null pour prendr en compte toutes
	 *            les dates depuis la date de début. Si les deux dates sont null
	 *            alors par défault toutes les dates sont prisent en compte. Si
	 *            les deux dates sont identiques, seul ce jour est pris en
	 *            compte.
	 * @return List<StagiaireAbs> une liste d'objet StagiaireAbs.
	 * @throws SQLException
	 */
	public List<StagiaireAbs> listePresentAbsent(Integer coursId, Boolean absPres, Date dateDebut, Date dateFin)
			throws SQLException {
		List<StagiaireAbs> listAbsent = new ArrayList<>();
		if ((this.cxt != null) && !this.cxt.isClosed()) {
			PreparedStatement request = null;
			ResultSet resultat = null;
			try {
				StringBuilder requete = new StringBuilder(
						"select nom, prenom, dateSignature, `date` from calendrier, utilisateur where absent=? and role='stagiaire'");
				if (coursId != null) {
					requete.append(" and Cours_id=?");
				}
				if (dateDebut != null) {
					requete.append(" and `date`>=?");
				}
				if (dateFin != null) {
					requete.append(" and `date`<=?");
				}
				requete.append(" and Utilisateur_id=utilisateur.id;");
				request = this.cxt.prepareStatement(requete.toString());
				int intParam = 1;
				request.setBoolean(intParam, absPres.booleanValue());
				intParam++;
				if (coursId != null) {
					request.setInt(intParam, coursId.intValue());
					intParam++;
				}
				if (dateDebut != null) {
					request.setDate(intParam, dateDebut);
					intParam++;
				}
				if (dateFin != null) {
					request.setDate(intParam, dateFin);
				}

				resultat = request.executeQuery();
				while (resultat.next()) {
					String unNom = resultat.getString("nom");
					String unPrenom = resultat.getString("prenom");
					Timestamp dateSignature = resultat.getTimestamp("dateSignature");
					Date dateCours = resultat.getDate("date");
					listAbsent.add(new StagiaireAbs(unNom, unPrenom, dateSignature, dateCours));
				}

			} catch (SQLException e) {
				throw e;
			} finally {
				if (resultat != null) {
					try {
						resultat.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				if (request != null) {
					try {
						request.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
			return listAbsent;
		} else {
			throw new SQLException("Connexion invalide!");
		}

	}

	/**
	 * Méthode pour attribuer des cours à un utilisateur, à condition qu'ils
	 * soient postérieur à la date du jour. On ne peut pas inscrire un
	 * utilisateur à un cours passé.
	 * 
	 * @param idUtil
	 *            l'identifiant de l'utilisateur.
	 * @param idCours
	 *            l'identifiant du cours.
	 * @return List<Integer> la liste des status d'insertion dans la base de
	 *         donnée. Sa longueur est égale aux nombre de jours composant le
	 *         cours. Chaque status renvoie 1 si l'insertion en base est réussi,
	 *         0 sinon.
	 * @throws SQLException
	 *             si un problème est rencontré avec la base de données.
	 * @throws EpresenceException
	 *             si le cours est terminé à cette date.
	 */
	public List<Integer> attribuerCours(Integer idUtil, Integer idCours) throws SQLException, EpresenceException {
		if ((this.cxt != null) && !this.cxt.isClosed()) {
			PreparedStatement request = null;
			ResultSet resultat = null;
			PreparedStatement request1 = null;
			List<Integer> resultat1 = new ArrayList<>();
			try {
				List<Calendrier> listCal = new ArrayList<>();
				request = this.cxt.prepareStatement(
						"select * from calendrier where Cours_id=? and `date`>=curdate() group by `date`;");
				request.setInt(1, idCours.intValue());
				resultat = request.executeQuery();
				while (resultat.next()) {
					Integer coursId = Integer.valueOf(resultat.getInt("Cours_id"));
					Date jours = resultat.getDate("date");
					listCal.add(new Calendrier(jours, null, null, null, coursId.intValue(), idUtil.intValue()));
				}
				if (listCal.isEmpty()) {
					throw new EpresenceException("Ce cours est terminé ID=" + idCours);
				}
				for (Calendrier calendrier : listCal) {
					request1 = this.cxt
							.prepareStatement("insert into calendrier set Cours_id=?, Utilisateur_id=?, `date`=?");
					request1.setInt(1, calendrier.getCoursId());
					request1.setInt(2, calendrier.getUtilisateurId());
					request1.setDate(3, new Date(calendrier.getDate().getTime()));
					resultat1.add(Integer.valueOf(request1.executeUpdate()));
				}

			} catch (SQLException e) {
				throw e;
			} finally {
				if (resultat != null) {
					try {
						resultat.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				if (request != null) {
					try {
						request.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
			return resultat1;
		} else {
			throw new SQLException("Connexion invalide!");
		}

	}
}
