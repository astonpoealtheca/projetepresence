/**
 * 
 */
package fr.bd;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import fr.presence.EpresenceException;
import fr.presence.Formateur;
import fr.presence.Stagiaire;
import fr.presence.Utilisateur;

/**
 * M�thode qui h�rite de AccesBD
 * @author Hatim ZINOUNE
 *
 */
public class AccesBdUtilisateur extends AccesBD{


	/**
	 * constructeur qui fait appele au constructeur de AccesBD
	 * @param aDriverName
	 * @throws SQLException
	 */
	public AccesBdUtilisateur(String aDriverName) throws SQLException {
		super(aDriverName);
	}

	/**
	 * Methode qui verifie que le login et le password vont bien ensemble. <br/>
	 *
	 * @param unMail
	 *            un login
	 * @param unMdp
	 *            un mot de passe
	 * @return
	 * 		<ul>
	 *         <li>-1: si un probleme provient du login</li>
	 *         <li>-2: si un probleme provient du mot de passe</li>
	 *         <li>l'id de l'utilisateur: si tout se passe bien</li>
	 *         </ul>
	 * @throws SQLException
	 *             si une erreur survient
	 */
	public int authentifier(String unMail, String unMdp) throws SQLException {
		if ((this.cxt != null) && !this.cxt.isClosed()) {
			PreparedStatement request = null;
			ResultSet resultat = null;
			try {
				// Creation de l'objet de requete
				request = this.cxt.prepareStatement("select id, CAST(AES_DECRYPT(motdepasse, 'MySecret') AS CHAR(50)) motdepasse_decrypt from utilisateur where mail=?");
				request.setString(1, unMail);

				// Envoie de la requete et recuperation du resultat
				resultat = request.executeQuery();

				// Parcours du resultat, toujours commencer par un .next
				while (resultat.next()) {
					Integer id = resultat.getInt("id");
					boolean noid = resultat.wasNull();
					String password = resultat.getString("motdepasse_decrypt");
					if (noid) {
						return -1;
					}
					return (password == unMdp) || password.equals(unMdp) ? id : -2;
				}
				return -1;
			} catch (SQLException sql) {
				// On la relance
				throw sql;
			} finally {
				// Tres IMPORTANT, on ferme tout
				if (resultat != null) {
					try {
						resultat.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				if (request != null) {
					try {
						request.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		} else {
			throw new SQLException("Connexion invalide!");
		}
	}

	/**
	 * M�thode pour modifier le mail
	 * @param id
	 * 			identifiant de l utilisateur
	 * @param nvMail
	 * 			nouveau mail
	 * @throws SQLException
	 */
	public void modifierMail(int id, String nvMail) throws SQLException{
		if ((this.cxt != null) && !this.cxt.isClosed()) {
			Statement request = null;
			ResultSet resultat = null;
			try {
				// Creation de l'objet de requete
				request = this.cxt.createStatement();
				request.executeUpdate("update Utilisateur set mail="+nvMail+" where id="+id);
				request.close();
			} catch (SQLException sql) {
				// On la relance
				throw sql;
			} finally {
				// Tres IMPORTANT, on ferme tout
				if (resultat != null) {
					try {
						resultat.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				if (request != null) {
					try {
						request.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		} else {
			throw new SQLException("Connexion invalide!");
		}
	}

	/**
	 * une m�thode pour modifier le mot de passe
	 * @param id
	 * 			identifiant de l utilisateur
	 * @param nvMdp
	 * 			nouveau mot de passe
	 * @param nvMdpConfimer
	 * 			nouveau mot de passe confirm�
	 * @return -3 si l id n existe pas dans la base 
	 * @return -2 si le mot de passe entré n'est pas le mot de passe actuel.
	 * @return -1 si le nouveau mot de passe et sa confirmation sont différent.
	 * @return 1 si le changement de mot de passe à réussi.
	 * @throws SQLException
	 */
	public int modifierMdp(int id, String nvMdp, String nvMdpConfimer) throws SQLException{
		if ((this.cxt != null) && !this.cxt.isClosed()) {
			PreparedStatement request = null;
			ResultSet resultat = null;
			Statement request2 = null;
			try {
				// Creation de l'objet de requete
				request = this.cxt.prepareStatement("select nom, prenom, mail, CAST(AES_DECRYPT(motdepasse, 'MySecret') AS CHAR(50)) motdepasse_decrypt from Utilisateur where id=?");
				request.setInt(1, id);

				// Envoie de la requete et recuperation du resultat
				resultat = request.executeQuery();

				// Parcours du resultat, toujours commencer par un .next
				while (resultat.next()) {
					String unNom = resultat.getString("nom");
					boolean noid = resultat.wasNull();
					String unPrenom = resultat.getString("prenom");
					String unMail = resultat.getString("mail");
					String unMdp = resultat.getString("motdepasse_decrypt");

					if (noid) {
						return -3; //si un probleme provient de l'id
					}

					//utilisation de la m�thode modifier mdp qui existe dans la classe m�tier Utilisateur (fr.presence)
					Utilisateur unUtilisateur = new Utilisateur(unNom, unPrenom, unMail, Integer.valueOf(id), unMdp,
							null);
					int mdpModifier = unUtilisateur.modifierMdp(unMdp, nvMdp, nvMdpConfimer);
					if(mdpModifier == 1){
						request2 = this.cxt.createStatement();
						request2.executeUpdate("update Utilisateur set motdepasse=AES_ENCRYPT('" + nvMdp
								+ "', 'MySecret') where id=" + id);
						request2.close();
					}
					return mdpModifier;
				}
				return -3;
			} catch (SQLException sql) {
				// On la relance
				throw sql;
			} finally {
				// Tres IMPORTANT, on ferme tout
				if (resultat != null) {
					try {
						resultat.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				if (request != null) {
					try {
						request.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		} else {
			throw new SQLException("Connexion invalide!");
		}

	}

	/**
	 * 
	 * @param id
	 * 			identifiant utilisateur
	 * @param idCours
	 * 			identifiant cours
	 * @return la cl�: si le code est bien g�n�r�
	 * @return -1: si probleme (si on trouve pas id ou idCours ou meme si il essaye avant ou apr�s la date du cours)
	 * @return -2 : pas de cours a cette date 
	 * @throws SQLException
	 */
	// @SuppressWarnings("unused")
	public String genererCode(int id, int idCours) throws SQLException{

		if ((this.cxt != null) && !this.cxt.isClosed()) {
			PreparedStatement request = null;
			PreparedStatement request1 = null;
			Statement request2 = null;
			ResultSet resultat = null;
			ResultSet resultat1 = null;
			try {
				// Creation de l'objet de requete
				request = this.cxt.prepareStatement("select nom, prenom, mail, role, CAST(AES_DECRYPT(motdepasse, 'MySecret') AS CHAR(50)) motdepasse_decrypt from Utilisateur where id=?");
				request.setInt(1, id);
				request1 = this.cxt.prepareStatement("select cleSignature from Calendrier where Cours_id =? and date = curdate()");
				//request1.setInt(1, id);
				request1.setInt(1, idCours);

				// Envoie de la requete et recuperation du resultat
				resultat = request.executeQuery();
				resultat1 = request1.executeQuery();

				// Parcours du resultat, toujours commencer par un .next

				while(resultat1.next()){
					String uneCle = resultat1.getString("cleSignature");
					boolean noid1 = resultat1.wasNull();
					if (!noid1) {
						return uneCle;
					}else{
						while (resultat.next()) {

							String unRole = resultat.getString("role");
							boolean noid = resultat.wasNull();
							if (noid) {
								return "-1";
							}
							String unNom = resultat.getString("nom");
							String unPrenom = resultat.getString("prenom");
							String unMail = resultat.getString("mail");
							String unMdp = resultat.getString("motdepasse_decrypt");
							if(unRole.equals("formateur")){
								Formateur unFormateur = new Formateur(unNom, unPrenom, unMail, Integer.valueOf(id),
										unMdp, null);
								String cleSignature = unFormateur.randomString(8); // 8 c est la longueur de la cl�
								request2 = this.cxt.createStatement();
								request2.executeUpdate("update Calendrier set cleSignature='"+ cleSignature +"' where Cours_id = "+idCours+" and Calendrier.date = curdate() and cleSignature is null");
								request2.close();
								return cleSignature;
							}

						}
					}
				}

				return "-2";
			} catch (SQLException sql) {
				// On la relance
				throw sql;
			} finally {
				// Tres IMPORTANT, on ferme tout
				if (resultat != null) {
					try {
						resultat.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				if (resultat1 != null) {
					try {
						resultat1.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				if (request != null) {
					try {
						request.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				if (request1 != null) {
					try {
						request1.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		} else {
			throw new SQLException("Connexion invalide!");
		}

	}

	/**
	 * Méthode permettant à un Stagiaire de signer sa présence dans un cours à
	 * l'aide de la clé de signature.
	 * 
	 * @param coursId
	 *            identifiant du cours
	 * @param utilisateurId
	 *            indentifiant du Stagiaire
	 * @param dateJour
	 *            date du jour de cours
	 * @param cleSignature
	 *            clé de signature fourni par le Formateur
	 * @return 1 si la signature a réussi
	 * @return 0 si la signature a échoué
	 * @throws Exception
	 */
	public Integer signer(Integer coursId, Integer utilisateurId, Date dateJour, String cleSignature, String adresseIp)
			throws SQLException, EpresenceException {
		if (coursId == null || utilisateurId == null || dateJour == null || cleSignature == null || adresseIp == null) {
			throw new EpresenceException(
					"Veuillez saisir cinq paramètres non null (Integer coursId, Integer utilisateurId, Date dateJour, String cleSignature, String adresseIp)");
		}
		if (this.cxt != null && !this.cxt.isClosed()) {
			PreparedStatement request = null;
			Integer resultat = null;
			try {
				String requete = "update calendrier set absent=?, dateSignature=?, ipSignature=? where Cours_id=? and Utilisateur_id=? and `date`=? and cleSignature=?";
				request = this.cxt.prepareStatement(requete);
				request.setBoolean(1, false);
				request.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
				request.setString(3, adresseIp);
				request.setInt(4, coursId.intValue());
				request.setInt(5, utilisateurId.intValue());
				request.setDate(6, dateJour);
				request.setString(7, cleSignature);
				resultat = Integer.valueOf(request.executeUpdate());
			} catch (SQLException e) {
				throw e;
			} finally {
				if (request != null) {
					try {
						request.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
			return resultat;
		} else {
			throw new SQLException("Connexion invalide!");
		}
	}

	/**
	 * Méthode pour récupérer la liste de tous les stagiaires de l'école
	 * 
	 * @return List<Stagiaire> la liste des stagiaires.
	 * @throws SQLException
	 */
	public List<Stagiaire> selectAllStagiaire() throws SQLException {
		List<Stagiaire> listStag = new ArrayList<>();
		if ((this.cxt != null) && !this.cxt.isClosed()) {
			PreparedStatement request = null;
			ResultSet resultat = null;
			try {
				// Creation de l'objet de requete
				request = this.cxt.prepareStatement(
						"select nom, prenom, mail, id, dateInscription from utilisateur where role='stagiaire'");

				// Envoie de la requete et recuperation du resultat
				resultat = request.executeQuery();

				// Parcours du resultat, toujours commencer par un .next
				while (resultat.next()) {
					String nom = resultat.getString("nom");
					String prenom = resultat.getString("prenom");
					String mail = resultat.getString("mail");
					Integer id = Integer.valueOf(resultat.getInt("id"));
					Date dateInscription = resultat.getDate("dateInscription");

					listStag.add(new Stagiaire(nom, prenom, mail, id, null, dateInscription));
				}
				return listStag;
			} catch (SQLException sql) {
				// On la relance
				throw sql;
			} finally {
				// Tres IMPORTANT, on ferme tout
				if (resultat != null) {
					try {
						resultat.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				if (request != null) {
					try {
						request.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		} else {
			throw new SQLException("Connexion invalide!");
		}
	}

	/**
	 * methode pour afficher les information de l'utilmisateur
	 * 
	 * @param id
	 *            identifiant de l'utilisateur
	 * @return une liste : nom, prenom, mail, role
	 * @return null : si id introuvable
	 * @throws SQLException
	 * @throws EpresenceException
	 */
	public Utilisateur selectUtilisateur(int id) throws SQLException {
		if ((this.cxt != null) && !this.cxt.isClosed()) {
			PreparedStatement request = null;
			ResultSet resultat = null;
			try {
				// Creation de l'objet de requete
				request = this.cxt.prepareStatement("select nom, prenom, mail, role, dateInscription from utilisateur where id=?");
				request.setInt(1, id);

				// Envoie de la requete et recuperation du resultat
				resultat = request.executeQuery();
				// Parcours du resultat, toujours commencer par un .next
				Utilisateur utilisateur = null;
				while (resultat.next()) {
					String nom = resultat.getString("nom");
					String prenom = resultat.getString("prenom");
					String mail = resultat.getString("mail");
					String role = resultat.getString("role");
					Date dateInscription = resultat.getDate("dateInscription");

					if (role.equals("stagiaire")) {
						utilisateur = new Stagiaire(nom, prenom, mail, Integer.valueOf(id), null, dateInscription);
					} else if (role.equals("formateur")) {
						utilisateur = new Formateur(nom, prenom, mail, Integer.valueOf(id), null, dateInscription);
					} else if (role.equals("administrateur")) {
						utilisateur = new Administrateur(nom, prenom, mail, Integer.valueOf(id), null, dateInscription);
					}
				}
				return utilisateur;
			} catch (SQLException sql) {
				// On la relance
				throw sql;
			} finally {
				// Tres IMPORTANT, on ferme tout
				if (resultat != null) {
					try {
						resultat.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				if (request != null) {
					try {
						request.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		} else {
			throw new SQLException("Connexion invalide!");
		}
	}

	/**
	 * Méthode pour récupérer l'id d'un utilisateur grace à son adresse mail
	 * 
	 * @param mail
	 *            adresse mail de l'utilisateur
	 * @return id l'identifiant de l'utilisateur
	 * @throws SQLException
	 *             si quelque chose se passe mal
	 */
	public Integer selectUtilisateurId(String mail) throws SQLException {
		if ((this.cxt != null) && !this.cxt.isClosed()) {
			PreparedStatement request = null;
			ResultSet resultat = null;
			try {
				// Creation de l'objet de requete
				request = this.cxt.prepareStatement("select id from utilisateur where mail=?");
				request.setString(1, mail);;

				// Envoie de la requete et recuperation du resultat
				resultat = request.executeQuery();

				// Parcours du resultat, toujours commencer par un .next
				Integer id = null;
				while (resultat.next()) {
					id = Integer.valueOf(resultat.getInt("id"));
				}
				return id;
			} catch (SQLException sql) {
				// On la relance
				throw sql;
			} finally {
				// Tres IMPORTANT, on ferme tout
				if (resultat != null) {
					try {
						resultat.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				if (request != null) {
					try {
						request.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		} else {
			throw new SQLException("Connexion invalide!");
		}
	}

	/**
	 * Methode pour calculer le nombre d absences d un stagiaire
	 * @param id
	 * 			identifiant stagiaire
	 * @return le nombre d'absence
	 * @throws SQLException
	 */
	public int calculerNbrAbsences(int id) throws SQLException{
		if ((this.cxt != null) && !this.cxt.isClosed()) {
			PreparedStatement request = null;
			ResultSet resultat = null;
			try {
				// Creation de l'objet de requete
				request = this.cxt.prepareStatement("select count(absent) from epresencetest.calendrier where Utilisateur_id=? and absent=1");
				request.setInt(1, id);

				// Envoie de la requete et recuperation du resultat
				resultat = request.executeQuery();

				// Parcours du resultat, toujours commencer par un .next
				while (resultat.next()) {
					int nbrAbsents = resultat.getInt(1);

					return nbrAbsents;
				}
				return -1;
			} catch (SQLException sql) {
				// On la relance
				throw sql;
			} finally {
				// Tres IMPORTANT, on ferme tout
				if (resultat != null) {
					try {
						resultat.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				if (request != null) {
					try {
						request.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		} else {
			throw new SQLException("Connexion invalide!");
		}
	}
	/**
	 * methode pour afficher les jours d absence d un stagiaire
	 * @param id
	 * 			identifiant stagiaire
	 * @return les dates d absences
	 * @throws SQLException
	 */
	public List<Date> calculerJoursAbsences(int id) throws SQLException{
		if ((this.cxt != null) && !this.cxt.isClosed()) {
			PreparedStatement request = null;
			ResultSet resultat = null;
			try {
				// Creation de l'objet de requete
				request = this.cxt.prepareStatement("select date from epresencetest.calendrier where Utilisateur_id=? and absent=1");
				request.setInt(1, id);

				// Envoie de la requete et recuperation du resultat
				resultat = request.executeQuery();

				// Parcours du resultat, toujours commencer par un .next
				List<Date> joursAbs = null;
				while (resultat.next()) {
					joursAbs = new ArrayList<Date>();
					joursAbs.add(resultat.getDate("date"));

					return joursAbs;
				}
				return joursAbs;
			} catch (SQLException sql) {
				// On la relance
				throw sql;
			} finally {
				// Tres IMPORTANT, on ferme tout
				if (resultat != null) {
					try {
						resultat.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				if (request != null) {
					try {
						request.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		} else {
			throw new SQLException("Connexion invalide!");
		}
	}

	/**
	 * Méthode pour inscrire un nouvel utilisateur dans la base de donnée. Cette
	 * méthode génère automatiquement un mot de passe aléatoire de 10 caractères
	 * et enregistre la date d'inscription.
	 * 
	 * @param unNom
	 *            nom de l'utilisateur
	 * @param unPrenom
	 *            prenom de l'utilisateur
	 * @param unMail
	 *            mail de l'utilisateur
	 * @param role
	 *            role de l útilisateur à choir parmis stagiaire, formateur ou
	 *            administrateur
	 * @return une list contenant deux objet. Le premier l'id de l'utilisateur
	 *         si l'opération à réussi, null sinon. Le deuxième le mot de passe
	 *         générer pour l'utilisateur si réussi, null sinon.
	 * @throws Exception
	 *             renvoie une exception en cas de mauvaise définition des
	 *             paramètre ou d'un problème avec la base de données.
	 */
	public List<Object> inscrire(String unNom, String unPrenom, String unMail, String role) throws Exception {
		if (unNom == null || unPrenom == null || unMail == null || role == null) {
			throw new EpresenceException(
					"Veuillez saisir quatre paramètres non null (String unNom, String unPrenom, String unMail, String role)");
		}
		if (role != "stagiaire" && role != "formateur" && role != "administrateur") {
			throw new EpresenceException("Veuillez choisir un role parmis : stagiaire, formateur ou administrateur");
		}
		if ((this.cxt != null) && !this.cxt.isClosed()) {
			PreparedStatement request = null;
			Integer res = null;

			ResultSet resultat = null;
			PreparedStatement request1 = null;
			String pwd = null;
			List<Object> listReturn = new ArrayList<>();
			try {
				request = this.cxt.prepareStatement(
						"insert into utilisateur set nom=?, prenom=?, mail=?, role=?, motdepasse=AES_ENCRYPT(?, 'MySecret'), dateInscription=now()");
				request.setString(1, unNom);
				request.setString(2, unPrenom);
				request.setString(3, unMail);
				request.setString(4, role);
				Formateur form = new Formateur();
				pwd = form.randomString(10);
				request.setString(5, pwd);
				request.executeUpdate();

				request1 = this.cxt.prepareStatement("select id from utilisateur where mail=?");
				request1.setString(1, unMail);

				resultat = request1.executeQuery();
				while (resultat.next()) {
					res = Integer.valueOf(resultat.getInt("id"));
					if (resultat.wasNull()) {
						res = null;
					}
				}

			} catch (SQLException e) {
				throw e;
			} finally {
				if (request != null) {
					try {
						request.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
			listReturn.add(res);
			listReturn.add(pwd);
			return listReturn;
		} else {
			throw new SQLException("Connexion invalide!");

		}
	}

}