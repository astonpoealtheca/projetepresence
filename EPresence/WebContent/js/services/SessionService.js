'use strict';

//service qui stocke les donnees de l'utilisateur dans une session
ePresence.service('Session', function(){

	/*var userId = null;
	return {
		getUserId : function(){
			return userId;
		},
    	create : function(newUserId){
        userId=newUserId;
    	},
    	destroy:function(){
        userId=null;
    	}
  	};*/
  	this.setValue = function (key, value) {
  		//console.log(value);
  		sessionStorage.setItem(key, value);
  	};
  	this.getValue = function(key) {
  		//console.log(key)
        return sessionStorage.getItem(key);
    };
    this.destroyItem = function(key) {
        sessionStorage.removeItem(key);
    };
});