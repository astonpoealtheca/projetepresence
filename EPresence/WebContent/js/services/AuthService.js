'use strict';

ePresence.factory('AuthService', ['$http', 'Session', '$rootScope','AUTH_EVENTS',
    function ($http, Session, $rootScope, AUTH_EVENTS){
	var authService ={};

//creation de la fonction connexion
	authService.connexion = function (utilisateur){
		return $http
			.post('rest/login', utilisateur)
            .then(function(res){
                //mise à jour des donnees utilisateur dans la session
                //$scope.userId = res.data.id;
                Session.setValue("id",res.data.id);
                return res;
        }, function (res) {
            return res;
        });
};
    
    //verifier si l'utilisateur est authentifié
    authService.isAuthenticated = function(){
        return !!Session.userId;
    };

    authService.logout = function(){
        Session.destroy();
        $rootScope.$broadcast(AUTH_EVENTS.logoutSucess);
    };
    
    return authService;
}]);

ePresence.constant('AUTH_EVENTS', {
  loginSuccess: 'auth-login-success',
  loginFailed: 'auth-login-failed',
  logoutSuccess: 'auth-logout-success',
  sessionTimeout: 'auth-session-timeout',
  notAuthenticated: 'auth-not-authenticated',
  notAuthorized: 'auth-not-authorized'
});