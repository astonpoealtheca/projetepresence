'use strict';

ePresence.controller('formateurController', ['$scope','$http','Session', '$rootScope', function($scope,$http,Session, $rootScope){
	$scope.userId = Session.getValue("id");
	$http.get('rest/infoUtilisateur?userId='+$scope.userId).then(function (response) {
		console.log(response);
		console.log(response.data);
		$scope.userData = response.data;
	}, function (response) {
		console.log(response.data);
		$scope.erreur = response.data;
	})
}]);