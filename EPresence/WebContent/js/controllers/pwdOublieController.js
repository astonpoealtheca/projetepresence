'use strict';

ePresence.controller('pwdOublieController', ['$scope', '$http', function($scope, $http){
    $scope.user = {
        mail:''
    };

    $scope.pwdOublie = function(){
        return $http
                .put('rest/pwdOublie', $scope.user)
                .then(function(res){
                    console.log(res);
                    console.log(res.data);
                    $scope.mailEnvoye=res.data;
                    $scope.mailOk=true;
    
            },  function(res){
                    console.log(res);
                    console.log(res);
                    $scope.erreur = res.data;
                    $scope.mailKo=true;
            
        });
        
    };
    
}]);

