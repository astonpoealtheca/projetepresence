'use strict';

ePresence.controller('coursController',['$scope', '$http', function ($scope, $http) {
    $scope.coursstagiaire = function(idDetails){
    	$http
        .get('rest/detailsCours?stagiaireId='+idDetails)
        .then(
            function(reponse){
                $scope.stagiaireCours = reponse.data;
            },
            function(reponse){
                $scope.erreur = response.data;
            }
        )
    }

    
}]);