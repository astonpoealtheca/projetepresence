'use strict';

ePresence.controller('adminController', ['$scope','$http','Session', '$rootScope', function($scope,$http,Session, $rootScope){
	$scope.userId = Session.getValue("id");
	$http.get('rest/infoUtilisateur?userId='+$scope.userId).then(function (response) {
		console.log(response);
		console.log(response.data);
		$scope.userData = response.data;
	}, function (response) {
		console.log(response.data);
		$scope.erreur = response.data;
	})
}]);


/*ePresence.controller('adminController', function ($scope) {

// liste déroulante multiselect de cours
	$scope.cours = listeCours;

	// tableaux de stagiaires
	$scope.stagiaires = listeStagiaires;
    
    // tableau de détail absence et/ou presence
    $scope.calendrier = listeCalendrier;


 $scope.example14model = [];
    $scope.example14settings = {
        scrollableHeight: '200px',
        scrollable: true,
        enableSearch: true
    };
    $scope.example14data = [{
        "label": "Javascript",
            "id": "JS"
    }, {
        "label": "Java J2EE",
            "id": "JE"
    }, {
        "label": "Java",
            "id": "J"
    }, {
        "label": "HTML5 CSS",
            "id": "HTML"
    }, {
        "label": "AngularJS",
            "id": "AS"
    }, {
        "label": "Gestion de projet",
            "id": "GP"
    }, {
        "label": "Gestion du stress",
            "id": "GS"
    }, {
        "label": "Base de données",
            "id": "BD"
    }, {
        "label": "Plan de test",
            "id": "PT"
    }
    
    ];
    $scope.example2settings = {
        displayProp: 'id'
    };
});/*
