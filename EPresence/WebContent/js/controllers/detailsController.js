'use strict';

ePresence.controller('detailsController',['$scope', '$http', function ($scope, $http) {
    $http
    .get('rest/details')
    .then(
        function(reponse){
            $scope.stagiaires = reponse.data;
        },
        function(reponse){
            $scope.erreur = response.data;
        }
    )
    $scope.abspres = "abs";
    
    $scope.chckerAbsPres = function(){
        if($scope.abspres == "abs"){
            $scope.absent = true;
            $scope.present = false;
        }else{
        	$scope.absent = false;
            $scope.present = true;
        }	
    }
    $scope.chckerAbsPres();

    
}]);

