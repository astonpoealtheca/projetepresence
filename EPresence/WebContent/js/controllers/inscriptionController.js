ePresence.controller('AppCtrl', function ($scope) {

 $scope.example14model = [];
    $scope.example14settings = {
        scrollableHeight: '200px',
        scrollable: true,
        enableSearch: true
    };
    $scope.example14data = [{
        "label": "Javascript",
            "id": "JS"
    }, {
        "label": "Java J2EE",
            "id": "JE"
    }, {
        "label": "Java",
            "id": "J"
    }, {
        "label": "HTML5 CSS",
            "id": "HTML"
    }, {
        "label": "AngularJS",
            "id": "AS"
    }, {
        "label": "Gestion de projet",
            "id": "GP"
    }, {
        "label": "Gestion du stress",
            "id": "GS"
    }
    
    ];
    $scope.example2settings = {
        displayProp: 'id'
    };
});