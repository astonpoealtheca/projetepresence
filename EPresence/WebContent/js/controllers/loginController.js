'use strict';

ePresence.controller('loginController', ['$scope', '$routeParams', '$location', '$window', 'AuthService', '$rootScope', 'AUTH_EVENTS',
 function($scope, $routeParams, $location, $window, AuthService,$rootScope, AUTH_EVENTS){
  // afficher le listing des régions
  $scope.regions = listeRegions;

  $scope.userId = $rootScope.userId;
    

// authentification utilisateur
    $scope.utilisateur = {
        mail : '',
        motdepasse : ''
     };
    
     var racineAdresse = "/EPresence";

    $scope.connexion = function(utilisateur){
        AuthService.connexion(utilisateur).then(function (user){
            $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
            if (user.status != 200) {
              console.log(user);
              $scope.erreur = user.data || "Erreur";
              $scope.msgErreur = true;
            } else {
              $window.location.href = racineAdresse + user.data.url;
            }
     
        }, function(){
            $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
        });
    };

}]);