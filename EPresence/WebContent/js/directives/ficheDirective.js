'use strict';

ePresence.directive('stagiaire', function(){
	return{
		restrict :'E',
		scope :{
			nom : '@',
			prenom : '@',
			mail : '@',
			promo : '@'
		},
		template : "<div>" +
				   "<span> Nom : {{nom}} </span>" + "</br>" +
				   "<span> Prenom : {{prenom}} </span>" + "</br>" +
				   "<span> Promotion : {{promo}} </span>" + "</br>" +
				   "<span> Courriel : {{mail}} </span>" + "</br>" +
				   "</div>" + "</br>" +"</br>"
				   	
	};	

});

ePresence.directive('formateur', function(){
	return{
		restrict :'E',
		scope :{
			nom : '@',
			prenom : '@',
			mail : '@',
		},
		template : "<div>" +
				   "<span> Nom : {{nom}} </span>" + "</br>" +
				   "<span> Prenom : {{prenom}} </span>" + "</br>" +
				   "<span> Courriel : {{mail}} </span>" + "</br>" +
				   "</div>" + "</br>" +"</br>"
				   	
	};	

});

ePresence.directive('admin', function(){
	return{
		restrict :'E',
		scope :{
			nom : '@',
			prenom : '@',
			mail : '@',
		},
		template : "<div>" +
				   "<span> Nom : {{nom}} </span>" + "</br>" +
				   "<span> Prenom : {{prenom}} </span>" + "</br>" +
				   "<span> Courriel : {{mail}} </span>" + "</br>" +
				   "</div>" + "</br>" +"</br>"
				   	
	};	

});

ePresence.directive('inscription', function(){
	return{
		restrict :'E',
		scope :{
			nom : '@',
			prenom : '@',
			mail : '@',
		},
		template : "<div>" +
				   "<span> Nom : {{nom}} </span>" + "</br>" +
				   "<span> Prenom : {{prenom}} </span>" + "</br>" +
				   "<span> Courriel : {{mail}} </span>" + "</br>" +
				   "</div>" + "</br>" +"</br>"				   	
	};	
});