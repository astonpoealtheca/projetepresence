
    $('#calendar').fullCalendar({
            events: 
                [
                {
                title: 'Java',
                start: '2016-03-08',
                end: '2016-03-12'  
                },
                {
                    title:'Java',
                    start:'2016-02-09',
                    description: "Il s'agit d'un cours sur Java",
                    end: '2016-02-16'
                },
                {
                    title:'Java',
                    start:'2016-01-18',
                end: '2016-01-30' 
                },
                {
                    title:'Web Services',
                    start:'2016-03-16',
                end: '2016-03-20' 
                },
                {
                title: 'Java',
                start: '2016-03-03',
                end: '2016-03-04'  
                },
                {
                    title:'HTML',
                    start:'2016-01-01',
                end: '2016-01-02'
                },
                {
                    title:'Javascript',
                    start:'2016-01-01',
                end: '2016-01-01' 
                },
                {
                    title:'AngularJS',
                    start:'2016-01-01',
                end: '2016-01-01' 
                },
                {
                    title:'NodeJS',
                    start:'2016-01-01',
                end: '2016-01-01'
                },
                {
                    title:'Video',
                    start:'2016-01-01',
                end: '2016-01-01'  
                },
                {
                    title:'Photo',
                    start:'2016-01-01',
                end: '2016-01-01' 
                },
                {
                    title:'Chant',
                    start:'2016-01-01',
                end: '2016-01-01'
                }
            ] ,
            color: 'yellow',
            textColor: 'black',
            weekends: false, // will hide Saturdays and Sundays
            theme:true,
            themeButtonIcons:{
                    prev: 'circle-triangle-w',
                    next: 'circle-triangle-e',
                    prevYear: 'seek-prev',
                    nextYear: 'seek-next'
                },
           height:700,
           firstDay: 1,
           eventLimit: 5,
           allDaySlot:true,
           header:{
                 left: 'month basicWeek ',
                 center: 'title',
                 right: 'today prev,next'},
         
        
          
});
